package com.bilinedev.coreframework.util;

import android.text.TextUtils;

/**
 * Created by pristyanchandra on 10/14/16.
 */

public class ValidationUtil {
    public static boolean isValidEmail(CharSequence target) {
        return !TextUtils.isEmpty(target) && android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
    }

    public static boolean lengthValidation(String text, int maxLength) {
        if (text.length() <= maxLength) {
            return true;
        } else {
            return false;
        }
    }

    public static boolean cardValidation(String ccNumber) {
        int sum = 0;
        boolean alternate = false;
        for (int i = ccNumber.length() - 1; i >= 0; i--) {
            int n = Integer.parseInt(ccNumber.substring(i, i + 1));
            if (alternate) {
                n *= 2;
                if (n > 9) {
                    n = (n % 10) + 1;
                }
            }
            sum += n;
            alternate = !alternate;
        }
        return (sum % 10 == 0);
    }
}
