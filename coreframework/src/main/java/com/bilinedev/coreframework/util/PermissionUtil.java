package com.bilinedev.coreframework.util;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;

import com.bilinedev.coreframework.ui.dialog.MessageDialog;

/**
 * Created by asus on 23/08/2016.
 */
public class PermissionUtil {

    public static boolean isCameraGranted(Context context, final Activity activity, FragmentManager fragmentManager, final int requestCode) {
        if (ContextCompat.checkSelfPermission(context, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED
                && ContextCompat.checkSelfPermission(context, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
                && ContextCompat.checkSelfPermission(context, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            if (ActivityCompat.shouldShowRequestPermissionRationale(activity, Manifest.permission.CAMERA)
                    || ActivityCompat.shouldShowRequestPermissionRationale(activity, Manifest.permission.READ_EXTERNAL_STORAGE)
                    || ActivityCompat.shouldShowRequestPermissionRationale(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                MessageDialog dialog = DialogUtil.messageDialog(fragmentManager,
                        "Need permission to open camera and write to sdcard");
                dialog.setCancelable(false);
                dialog.setOnMessageClosed(new MessageDialog.OnMessageClosed() {
                    @Override
                    public void onClosed() {
                        requestCameraPermission(activity, requestCode);
                    }
                });
            } else {
                requestCameraPermission(activity, requestCode);
            }
            return false;
        }
    }

    public static void requestCameraPermission(Activity activity, int requestCode) {
        ActivityCompat.requestPermissions(activity, new String[]{
                Manifest.permission.CAMERA,
                Manifest.permission.READ_EXTERNAL_STORAGE,
                Manifest.permission.WRITE_EXTERNAL_STORAGE}, requestCode);
    }

    public static boolean isLocationGranted(Context context, final Activity activity, FragmentManager fragmentManager, final int requestCode) {
        if (ContextCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED
                && ContextCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            if (ActivityCompat.shouldShowRequestPermissionRationale(activity, Manifest.permission.ACCESS_COARSE_LOCATION)
                    || ActivityCompat.shouldShowRequestPermissionRationale(activity, Manifest.permission.ACCESS_FINE_LOCATION)) {
                MessageDialog dialog = DialogUtil.messageDialog(fragmentManager,
                        "Need permission to access your location");
                dialog.setCancelable(false);
                dialog.setOnMessageClosed(new MessageDialog.OnMessageClosed() {
                    @Override
                    public void onClosed() {
                        requestLocationPermission(activity, requestCode);
                    }
                });
            } else {
                requestLocationPermission(activity, requestCode);
            }
            return false;
        }
    }

    public static void requestLocationPermission(Activity activity, int requestCode) {
        ActivityCompat.requestPermissions(activity, new String[]{
                Manifest.permission.ACCESS_COARSE_LOCATION,
                Manifest.permission.ACCESS_FINE_LOCATION}, requestCode);
    }



    public static boolean isStorageGranted(Context context, final Activity activity, FragmentManager fragmentManager, final int requestCode) {
        if (ContextCompat.checkSelfPermission(context, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
                && ContextCompat.checkSelfPermission(context, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            if (ActivityCompat.shouldShowRequestPermissionRationale(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    || ActivityCompat.shouldShowRequestPermissionRationale(activity, Manifest.permission.READ_EXTERNAL_STORAGE)) {
                MessageDialog dialog = DialogUtil.messageDialog(fragmentManager,
                        "Need permission to open storage");
                dialog.setCancelable(false);
                dialog.setOnMessageClosed(new MessageDialog.OnMessageClosed() {
                    @Override
                    public void onClosed() {
                        requestStoragePermission(activity,requestCode);
                    }
                });
            } else {
                requestStoragePermission(activity,requestCode);
            }
            return false;
        }
    }

    public static void requestStoragePermission(Activity activity, int requestCode) {
        ActivityCompat.requestPermissions(activity, new String[]{
                Manifest.permission.READ_EXTERNAL_STORAGE,
                Manifest.permission.WRITE_EXTERNAL_STORAGE}, requestCode);
    }

    public static boolean isCalendarGranted(Context context, final Activity activity, FragmentManager fragmentManager) {
        if (ContextCompat.checkSelfPermission(context, Manifest.permission.READ_CALENDAR) == PackageManager.PERMISSION_GRANTED
                && ContextCompat.checkSelfPermission(context, Manifest.permission.WRITE_CALENDAR) == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            if (ActivityCompat.shouldShowRequestPermissionRationale(activity, Manifest.permission.READ_CALENDAR)
                    || ActivityCompat.shouldShowRequestPermissionRationale(activity, Manifest.permission.WRITE_CALENDAR)) {
                MessageDialog dialog = DialogUtil.messageDialog(fragmentManager,
                        "Need permission to read and write to calendar");
                dialog.setCancelable(false);
                dialog.setOnMessageClosed(new MessageDialog.OnMessageClosed() {
                    @Override
                    public void onClosed() {
                        requestCalendarPermission(activity);
                    }
                });
            } else {
                requestCalendarPermission(activity);
            }
            return false;
        }
    }

    public static void requestCalendarPermission(Activity activity) {
        ActivityCompat.requestPermissions(activity, new String[]{
                Manifest.permission.READ_CALENDAR,
                Manifest.permission.WRITE_CALENDAR}, 0);
    }
}
