package com.bilinedev.coreframework.util;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.support.v4.content.FileProvider;
import android.support.v7.widget.PopupMenu;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;

import com.bilinedev.coreframework.R;
import com.bilinedev.coreframework.util.imagecompressor.Compressor;
import com.bilinedev.coreframework.util.imagecompressor.ImageUtilThumb;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.UUID;

/**
 * Created by pristyanchandra on 11/22/16.
 */

public class ImageUtil {

    private Activity activity;
    private String fileName;
    private String fileType;
    private String folderName;
    private File fileToSend;

    public static final String EXT_JPG = ".jpg";
    public static final String EXT_PNG = ".png";

    public static final int REQUEST_CAMERA = 111;
    public static final int REQUEST_GALLERY = 112;
    public static int REQUEST_CODE = 0;
    public static boolean CAMERA = false;
    public static boolean GALLERY = false;

    public static String DEFAULT_FOLDER_NAME = "BilineDev";
    public static String TEMP_FOLDER_NAME = "Temp";
    static Compressor.Builder builderOriginal;

    public ImageUtil(Activity activity) {
        this.activity = activity;
    }

    private String getFileName() {
        if (fileName == null) {
            fileName = "Biline_" + DateUtil.getCurrentDateTimeForPhoto();
        }
        return fileName;
    }

    private String getFileType() {
        if (fileType == null) {
            fileType = EXT_JPG;
        }
        return fileType;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public void setFileType(String fileType) {
        this.fileType = fileType;
    }

    public void openImageChooser(Activity activity, View anchorView) {
        PopupMenu popupMenu = new PopupMenu(activity, anchorView);
        popupMenu.getMenuInflater().inflate(R.menu.option_camera_ind, popupMenu.getMenu());
        popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                if (item.getItemId() == R.id.option_camera) {
                    openCamera();
                } else {
                    openGallery();
                }
                return false;
            }
        });
        popupMenu.show();
    }

    public void openImageChooser(Activity activity, View anchorView, final int requestCode) {
        REQUEST_CODE = requestCode;
        PopupMenu popupMenu = new PopupMenu(activity, anchorView);
        popupMenu.getMenuInflater().inflate(R.menu.option_camera_ind, popupMenu.getMenu());
        popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                if (item.getItemId() == R.id.option_camera) {
                    openCamera(requestCode);
                } else {
                    openGallery(requestCode);
                }
                return false;
            }
        });
        popupMenu.show();
    }
    public void setFolderName(String folderName) {
        this.folderName = folderName;
    }

    private String getFolderName() {
        if (folderName == null) {
            folderName = DEFAULT_FOLDER_NAME;
        }
        return folderName;
    }

    private File createImageFile() throws IOException {
        File storageDir = new File(Environment.getExternalStorageDirectory() + "/" + getFolderName());
        if (!storageDir.exists()) {
            storageDir.mkdirs();
        }

        return File.createTempFile(
                getFileName(),  /* prefix */
                getFileType(),  /* suffix */
                storageDir      /* directory */
        );
    }

    public void openCamera() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(activity.getPackageManager()) != null) {
            fileToSend = null;
            try {
                fileToSend = createImageFile();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
            if (fileToSend != null) {
                Uri photoURI = FileProvider.getUriForFile(activity, activity.getApplicationContext().getPackageName() + ".provider", fileToSend);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                activity.startActivityForResult(takePictureIntent, REQUEST_CAMERA);
            }
        }
    }

    public void openCamera(int requestCode) {
        CAMERA = true;
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(activity.getPackageManager()) != null) {
            fileToSend = null;
            try {
                fileToSend = createImageFile();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
            if (fileToSend != null) {
                Uri photoURI = FileProvider.getUriForFile(activity, activity.getApplicationContext().getPackageName() + ".provider", fileToSend);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                activity.startActivityForResult(takePictureIntent, requestCode);
            }
        }
    }

    public void openGallery() {
        Intent intent = new Intent("android.intent.action.PICK", MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        activity.startActivityForResult(intent, REQUEST_GALLERY);
    }

    public void openGallery(int requestCode) {
        GALLERY = true;
        Intent intent = new Intent("android.intent.action.PICK", MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        activity.startActivityForResult(intent, requestCode);
    }

    public File onHandleImage(int requestCode, int resultCode, Intent data) {
        try {
            if (requestCode == REQUEST_CAMERA && resultCode == activity.RESULT_OK) {
                return fileToSend;
            }

            if (CAMERA == true && requestCode == REQUEST_CODE && resultCode == activity.RESULT_OK) {
                Log.e("Camera", ""+REQUEST_CODE);
                CAMERA = false;
                if (fileToSend != null){
                    return fileToSend;
                } else {
                    return null;
                }
            }

            if (requestCode == REQUEST_GALLERY && resultCode == activity.RESULT_OK) {
                Uri e = data.getData();
                return pickedExistingPicture(activity, e);
            }

            if (GALLERY == true && requestCode == REQUEST_CODE && resultCode == activity.RESULT_OK) {
                Log.e("Gallery", ""+REQUEST_CODE);
                GALLERY = false;
                Uri e = data.getData();
                if (pickedExistingPicture(activity, e) != null){
                    return pickedExistingPicture(activity, e);
                } else {
                    return null;
                }
            }

            return null;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    public File pickedExistingPicture(Context context, Uri photoUri) throws IOException {
        InputStream pictureInputStream = context.getContentResolver().openInputStream(photoUri);
        File directory = tempImageDirectory(context);
        File photoFile = new File(directory, UUID.randomUUID().toString());
        photoFile.createNewFile();
        writeToFile(pictureInputStream, photoFile);
        return photoFile;
    }

    public static File compressImage(Context context, File file) {
        ImageUtilThumb imageUtilThumb = new ImageUtilThumb();
        return imageUtilThumb.compressImage(context, Uri.fromFile(file), 1000, 1000,
                Bitmap.CompressFormat.JPEG, 80, Environment.getExternalStorageDirectory().getPath());

        /*builderOriginal = new Compressor.Builder(context);
        return builderOriginal
                .setMaxWidth(1000)
                .setMaxHeight(1000)
                .setQuality(80)
                .setCompressFormat(Bitmap.CompressFormat.JPEG)
                .setDestinationDirectoryPath(Environment.getExternalStorageDirectory().getPath())
                .build()
                .compressToFile(file);*/
    }

    public static File compressImageThumbnail(Context context, File file) {
        ImageUtilThumb imageUtilThumb = new ImageUtilThumb();
        return imageUtilThumb.compressImage(context, Uri.fromFile(file), 100, 100,
                Bitmap.CompressFormat.JPEG, 10, Environment.getExternalStorageDirectory().getPath());
    }

    private void writeToFile(InputStream in, File file) {
        try {
            FileOutputStream e = new FileOutputStream(file);
            byte[] buf = new byte[1024];

            int len;
            while((len = in.read(buf)) > 0) {
                e.write(buf, 0, len);
            }

            e.close();
            in.close();
        } catch (Exception var5) {
            var5.printStackTrace();
        }

    }

    private File tempImageDirectory(Context context) {
        boolean publicTemp = PreferenceManager.getDefaultSharedPreferences(context).getBoolean("bilinedev.core.public_temp", false);
        File dir = publicTemp?publicTempDir(context):privateTempDir(context);
        if(!dir.exists()) {
            dir.mkdirs();
        }

        return dir;
    }

    private File publicTempDir(Context context) {
        File cameraPicturesDir = new File(getFolderLocation(context), getFolderName(context));
        File publicTempDir = new File(cameraPicturesDir, TEMP_FOLDER_NAME);
        if(!publicTempDir.exists()) {
            publicTempDir.mkdirs();
        }
        return publicTempDir;
    }

    private File privateTempDir(Context context) {
        File privateTempDir = new File(context.getApplicationContext().getCacheDir(), getFolderName(context));
        if(!privateTempDir.exists()) {
            privateTempDir.mkdirs();
        }
        return privateTempDir;
    }

    private String getFolderName(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).getString("bilinedev.core.folder_name", DEFAULT_FOLDER_NAME);
    }

    private String getFolderLocation(Context context) {
        String defaultFolderLocation = publicAppExternalDir(context).getPath();
        return PreferenceManager.getDefaultSharedPreferences(context).getString("bilinedev.core.folder_location", defaultFolderLocation);
    }

    private File publicAppExternalDir(Context context) {
        return context.getExternalFilesDir(Environment.DIRECTORY_PICTURES);
    }
}
