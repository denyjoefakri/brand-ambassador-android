package com.bilinedev.coreframework.util;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Build;
import android.support.v4.app.FragmentManager;
import android.view.Window;

import com.bilinedev.coreframework.ui.dialog.ConfirmationDialog;
import com.bilinedev.coreframework.ui.dialog.MessageDialog;
import com.bilinedev.coreframework.ui.dialog.MessageWithTitleDialog;

/**
 * Created by pristyanchandra on 10/14/16.
 */

public class DialogUtil {
    public static ProgressDialog createProgressDialog(Context context, String message, boolean cancelable) {
        ProgressDialog dialog = new ProgressDialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        }
        dialog.setMessage(message);
        dialog.setCancelable(cancelable);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
        return dialog;
    }

    public static MessageDialog messageDialog(FragmentManager fragmentManager, String message) {
        MessageDialog dialog = MessageDialog.newIntance(message);
        dialog.show(fragmentManager, "dialogMessage");
        return dialog;
    }

    public static MessageWithTitleDialog messageDialogWithTitle(FragmentManager fragmentManager, String title, String message) {
        MessageWithTitleDialog dialog = MessageWithTitleDialog.newIntance(title, message);
        dialog.show(fragmentManager, "dialogMessageWithTitle");
        return dialog;
    }

    public static ConfirmationDialog confirmationDialog(FragmentManager fragmentManager, String message) {
        ConfirmationDialog dialog = ConfirmationDialog.newIntance(message);
        dialog.show(fragmentManager, "dialogConfirmation");
        return dialog;
    }
}
