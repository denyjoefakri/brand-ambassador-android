package com.bilinedev.coreframework.util;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by pristyanchandra on 10/14/16.
 */

public class DateUtil {
    public static String getMonth(int month) {
        String res;
        switch (month) {
            case 0:
                res = "January";
                break;
            case 1:
                res = "February";
                break;
            case 2:
                res = "March";
                break;
            case 3:
                res = "April";
                break;
            case 4:
                res = "May";
                break;
            case 5:
                res = "June";
                break;
            case 6:
                res = "July";
                break;
            case 7:
                res = "August";
                break;
            case 8:
                res = "September";
                break;
            case 9:
                res = "October";
                break;
            case 10:
                res = "November";
                break;
            default:
                res = "December";
                break;
        }
        return res;
    }

    public static String getMonthInd(int month) {
        String res;
        switch (month) {
            case 0:
                res = "Januari";
                break;
            case 1:
                res = "Februari";
                break;
            case 2:
                res = "Maret";
                break;
            case 3:
                res = "April";
                break;
            case 4:
                res = "Mei";
                break;
            case 5:
                res = "Juni";
                break;
            case 6:
                res = "Juli";
                break;
            case 7:
                res = "Agustus";
                break;
            case 8:
                res = "September";
                break;
            case 9:
                res = "Oktober";
                break;
            case 10:
                res = "November";
                break;
            default:
                res = "Desember";
                break;
        }
        return res;
    }

    public static String getDay(int day) {
        String res;
        switch (day) {
            case 0:
                res = "Sunday";
                break;
            case 1:
                res = "Monday";
                break;
            case 2:
                res = "Tuesday";
                break;
            case 3:
                res = "Wednesday";
                break;
            case 4:
                res = "Thursday";
                break;
            case 5:
                res = "Friday";
                break;
            default:
                res = "Saturday";
                break;
        }
        return res;
    }

    public static String getDayInd(int day) {
        String res;
        switch (day) {
            case 0:
                res = "Minggu";
                break;
            case 1:
                res = "Senin";
                break;
            case 2:
                res = "Selasa";
                break;
            case 3:
                res = "Rabu";
                break;
            case 4:
                res = "Kamis";
                break;
            case 5:
                res = "Jumat";
                break;
            default:
                res = "Sabtu";
                break;
        }
        return res;
    }

    public static String getCurrentDate() {
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date date = new Date();
        return dateFormat.format(date);
    }

    public static String getTomorrowDate() {
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date date = new Date();
        Calendar c = Calendar.getInstance();
        c.add(Calendar.DATE, 1);
        return dateFormat.format(c.getTime());
    }

    public static String getFullDate(String dateInput) {
        String year = dateInput.substring(0, 4);
        String month = dateInput.substring(5, 7);
        String date = dateInput.substring(8, 10);
        int m = Integer.valueOf(month) - 1;
        return date + " " + getMonth(m) + " " + year;
    }

    public static Calendar getCurrentCalendar(String currenDate) {
        Calendar cal = Calendar.getInstance();
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            cal.setTime(sdf.parse(currenDate));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return cal;
    }

    public static long getMilis(String dateTime) {
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm a");
            Date date = sdf.parse(dateTime);
            return date.getTime();
        } catch (ParseException e) {
            e.printStackTrace();
            return 0;
        }
    }

    public static String getCurrentDateTime() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm a");
        Date date = new Date();
        return dateFormat.format(date);
    }

    public static String getCurrentDateTimeForPhoto() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd_HHmmss");
        Date date = new Date();
        return dateFormat.format(date);
    }

}
