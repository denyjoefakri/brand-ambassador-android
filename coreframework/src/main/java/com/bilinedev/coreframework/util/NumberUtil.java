package com.bilinedev.coreframework.util;

import java.text.NumberFormat;
import java.util.Locale;

/**
 * Created by pristyanchandra on 10/14/16.
 */

public class NumberUtil {
    public static String getFormatedNumber(int input) {
        NumberFormat numberFormat = NumberFormat.getNumberInstance(Locale.US);
        return numberFormat.format(input);
    }
}
