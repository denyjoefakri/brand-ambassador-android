package com.bilinedev.coreframework.connection;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;

import okhttp3.OkHttpClient;

/**
 * Created by pristyanchandra on 10/14/16.
 */
public class ServiceUtil {
    public static OkHttpClient client;
    public static final String MESSAGE_RESPONSE_RTO = "Connetion timeout. Make sure you have internet connection";

    public static String getParameter(HashMap<String, String> params) {
        if (params != null) {
            String parameter = "";
            for (String key : params.keySet()) {
                if (parameter.equals("")) {
                    parameter = key + "=" + params.get(key);
                } else {
                    parameter = parameter + "&" + key + "=" + params.get(key);
                }
            }
            return encodePass(parameter);
        } else {
            return "null";
        }
    }

    public static String getFile(HashMap<String, File> params) {
        if (params != null) {
            String parameter = "";
            for (String key : params.keySet()) {
                if (parameter.equals("")) {
                    parameter = key + "=" + params.get(key).getName();
                } else {
                    parameter = parameter + "&" + key + "=" + params.get(key).getName();
                }
            }
            return encodePass(parameter);
        } else {
            return "null";
        }
    }

    public static String getMultipleParameter(HashMap<String, ArrayList<String>> params) {
        if (params != null) {
            String parameter = "";
            for (String key : params.keySet()) {
                for (int i = 0; i < params.get(key).size(); i++) {
                    if (parameter.equals("")) {
                        parameter = key + "=" + params.get(key).get(i);
                    } else {
                        parameter = parameter + "&" + key + "=" + params.get(key).get(i);
                    }
                }
            }
            return encodePass(parameter);
        } else {
            return "null";
        }
    }

    public static String getMultipleParameterFile(HashMap<String, ArrayList<File>> params) {
        if (params != null) {
            String parameter = "";
            for (String key : params.keySet()) {
                for (int i = 0; i < params.get(key).size(); i++) {
                    if (parameter.equals("")) {
                        parameter = key + "=" + params.get(key).get(i).getName();
                    } else {
                        parameter = parameter + "&" + key + "=" + params.get(key).get(i).getName();
                    }
                }
            }
            return encodePass(parameter);
        } else {
            return "null";
        }
    }

    public static String getParameterWithMultipleParameter(HashMap<String, String> paramSingle, HashMap<String, ArrayList<String>> paramMultiple) {
        if (paramMultiple != null && paramSingle != null) {
            String parameter = "";
            String multipleParameter = "";

            for (String key : paramMultiple.keySet()) {
                for (int i = 0; i < paramMultiple.get(key).size(); i++) {
                    if (multipleParameter.equals("")) {
                        multipleParameter = key + "=" + paramMultiple.get(key).get(i);
                    } else {
                        multipleParameter = multipleParameter + "&" + key + "=" + paramMultiple.get(key).get(i);
                    }
                }
            }

            for (String key : paramSingle.keySet()) {
                if (parameter.equals("")) {
                    parameter = key + "=" + paramSingle.get(key);
                } else {
                    parameter = parameter + "&" + key + "=" + paramSingle.get(key);
                }
            }

            return encodePass(parameter) + "&"+ encodePass(multipleParameter);
        } else {
            return "null";
        }
    }

    public static String encodePass(String input){
        input = input.replaceAll("%20", "+");
        input = input.replaceAll("%3D", "=");
        input = input.replaceAll("%26", "&");
        return input;
    }
}
