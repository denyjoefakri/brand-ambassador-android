package com.bilinedev.coreframework.connection;

import java.io.File;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by pristyanchandra on 10/14/16.
 */
public class ServiceRequest implements Serializable {
    private String path;
    private String method;
    private String filter;
    private HashMap<String, String> parameter;
    private HashMap<String, File> files;
    private String fileName;
    private String key;
    private HashMap<String,ArrayList<String>> multipleParameter;
    private HashMap<String,ArrayList<File>> multipleFile;
    private boolean haveFile;

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getFilter() {
        return filter;
    }

    public void setFilter(String filter) {
        this.filter = filter;
    }

    public HashMap<String, String> getParameter() {
        return parameter;
    }

    public void setParameter(HashMap<String, String> parameter) {
        this.parameter = parameter;
    }

    public HashMap<String, File> getFiles() {
        return files;
    }

    public void setFiles(HashMap<String, File> files) {
        this.files = files;
    }

    public HashMap<String, ArrayList<String>> getMultipleParameter() {
        return multipleParameter;
    }

    public void setMultipleParameter(HashMap<String, ArrayList<String>> multipleParameter) {
        this.multipleParameter = multipleParameter;
    }

    public HashMap<String, ArrayList<File>> getMultipleFile() {
        return multipleFile;
    }

    public void setMultipleFile(HashMap<String, ArrayList<File>> multipleFile) {
        this.multipleFile = multipleFile;
    }

    public boolean isHaveFile() {
        return haveFile;
    }

    public void setHaveFile(boolean haveFile) {
        this.haveFile = haveFile;
    }
}
