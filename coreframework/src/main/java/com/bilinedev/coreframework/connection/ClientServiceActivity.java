package com.bilinedev.coreframework.connection;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;

import com.bilinedev.coreframework.util.DialogUtil;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by pristyanchandra on 10/14/16.
 */
public class ClientServiceActivity extends AppCompatActivity {

    public void sendGetRequest(String path, HashMap<String, String> parameter, String filter, String key) {
        ServiceRequest serviceRequest = new ServiceRequest();
        serviceRequest.setPath(path);
        serviceRequest.setMethod(ClientService.METHOD_GET);
        serviceRequest.setParameter(parameter);
        serviceRequest.setFilter(filter);
        serviceRequest.setKey(key);

        Intent i = new Intent(this, ClientService.class);
        i.putExtra(ClientService.TAG_REQUEST, serviceRequest);
        startService(i);
    }

    public void sendPostRequest(String path, HashMap<String, String> parameter, String filter, String key) {
        ServiceRequest serviceRequest = new ServiceRequest();
        serviceRequest.setPath(path);
        serviceRequest.setMethod(ClientService.METHOD_POST);
        serviceRequest.setParameter(parameter);
        serviceRequest.setFilter(filter);
        serviceRequest.setKey(key);

        Intent i = new Intent(this, ClientService.class);
        i.putExtra(ClientService.TAG_REQUEST, serviceRequest);
        startService(i);
    }

    public void sendPostWithFile(String path, HashMap<String, String> parameter, HashMap<String,File> file, String filter, String key) {
        ServiceRequest serviceRequest = new ServiceRequest();
        serviceRequest.setPath(path);
        serviceRequest.setMethod(ClientService.METHOD_POST);
        serviceRequest.setParameter(parameter);
        serviceRequest.setFilter(filter);
        serviceRequest.setFiles(file);
        serviceRequest.setKey(key);

        Intent i = new Intent(this, ClientService.class);
        i.putExtra(ClientService.TAG_REQUEST, serviceRequest);
        startService(i);
    }

    public void sendMultiplePostRequestWithFile(String path, HashMap<String, ArrayList<String>> parameter, HashMap<String,ArrayList<File>> file, String filter, String key) {
        ServiceRequest serviceRequest = new ServiceRequest();
        serviceRequest.setPath(path);
        serviceRequest.setMethod(ClientService.METHOD_POST);
        serviceRequest.setMultipleParameter(parameter);
        serviceRequest.setFilter(filter);
        serviceRequest.setMultipleFile(file);
        serviceRequest.setKey(key);

        Intent i = new Intent(this, ClientServiceMultiple.class);
        i.putExtra(ClientService.TAG_REQUEST, serviceRequest);
        startService(i);
    }

    public void sendPostRequestWithArrayParamFile(String path, HashMap<String, String> parameter, HashMap<String, ArrayList<File>> paramFileMultiple, String filter, String key){
        ServiceRequest serviceRequest = new ServiceRequest();
        serviceRequest.setPath(path);
        serviceRequest.setMethod(ClientService.METHOD_POST);
        serviceRequest.setParameter(parameter);
        serviceRequest.setFilter(filter);
        serviceRequest.setKey(key);
        if (paramFileMultiple !=null){
            serviceRequest.setMultipleFile(paramFileMultiple);
        }
        serviceRequest.setHaveFile(true);

        Intent i = new Intent(this, ClientService.class);
        i.putExtra(ClientService.TAG_REQUEST, serviceRequest);
        startService(i);
    }


    public BroadcastReceiver getReceiver() {
        return new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                ServiceResponse serviceResponse = (ServiceResponse) intent.getSerializableExtra(ClientService.TAG_RESPONSE);
                switch (serviceResponse.getCode()) {
                    case 10:
                        onTimeout(intent.getAction());
                        break;
                    case 200:
                    case 204:
                        successResponse(serviceResponse, intent.getAction());
                        break;
                    default:
                        if (serviceResponse.getMessage() == null || serviceResponse.getMessage().equals("null")) {
                            onTimeout(intent.getAction());
                        } else {
                            badResponse(serviceResponse, intent.getAction());
                        }
                        break;
                }
            }
        };
    }

    public void successResponse(ServiceResponse response, String filter){}

    public void badResponse(ServiceResponse response, String filter){}

    public void onTimeout(String filter){}
}
