package com.bilinedev.coreframework.connection;

import android.app.IntentService;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.HttpUrl;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * Created by pristyanchandra on 10/14/16.
 */
public class ClientService extends IntentService {

    public static final String TAG_RESPONSE = "serviceResponse";
    public static final String TAG_REQUEST = "serviceRequest";
    public static final String TAG_TIMEOUT = "Request Timeout";

    public static final String METHOD_GET = "GET";
    public static final String METHOD_POST = "POST";
    public static final MediaType TYPE_URL_ENCODED = MediaType.parse("application/x-www-form-urlencoded; charset=utf-8");
    private static final MediaType MEDIA_TYPE_JPG = MediaType.parse("image/jpg");

    public ClientService() {
        super("ClientService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        if (ServiceUtil.client == null) {
            ServiceUtil.client = new OkHttpClient.Builder()
                    .readTimeout(5, TimeUnit.MINUTES)
                    .writeTimeout(5, TimeUnit.MINUTES)
                    .connectTimeout(5, TimeUnit.MINUTES)
                    .build();
        }

        final ServiceRequest serviceRequest = (ServiceRequest) intent.getSerializableExtra(TAG_REQUEST);
        String url = serviceRequest.getPath() + serviceRequest.getKey();
        final String filter = serviceRequest.getFilter();
        String fullUrl;
        HttpUrl.Builder urlBuilder = HttpUrl.parse(url).newBuilder();
        Request request;
        RequestBody requestBody = null;

        if (serviceRequest.getFiles() == null && serviceRequest.getMultipleFile() == null) {
            if (serviceRequest.getMethod().equals(METHOD_POST)) {
                if (serviceRequest.getMultipleParameter() != null){
                    requestBody = RequestBody.create(TYPE_URL_ENCODED, ServiceUtil.getParameterWithMultipleParameter(serviceRequest.getParameter(),serviceRequest.getMultipleParameter()));
                    request = new Request.Builder()
                            .url(url)
                            .post(requestBody)
                            .build();
                    printLogRequest(url,
                            filter,
                            serviceRequest.getMethod(),
                            ServiceUtil.getParameterWithMultipleParameter(serviceRequest.getParameter(),serviceRequest.getMultipleParameter()),
                            false);
                }else {
                    requestBody = RequestBody.create(TYPE_URL_ENCODED, ServiceUtil.getParameter(serviceRequest.getParameter()));
                    request = new Request.Builder()
                            .url(url)
                            .post(requestBody)
                            .build();
                    printLogRequest(url,
                            filter,
                            serviceRequest.getMethod(),
                            ServiceUtil.getParameter(serviceRequest.getParameter()),
                            false);
                }
            } else {
                if (serviceRequest.getParameter() != null) {
                    for (String key : serviceRequest.getParameter().keySet()) {
                        urlBuilder.addQueryParameter(key, serviceRequest.getParameter().get(key));
                    }
                }
                fullUrl = urlBuilder.build().toString();
                request = new Request.Builder()
                        .url(fullUrl)
                        .build();
                printLogRequest(fullUrl,
                        filter,
                        serviceRequest.getMethod(),
                        ServiceUtil.getParameter(serviceRequest.getParameter()),
                        false);
            }
        } else {
            MultipartBody.Builder builder = new MultipartBody.Builder();
            builder.setType(MultipartBody.FORM);

            for (String key : serviceRequest.getParameter().keySet()) {
                builder.addFormDataPart(key, serviceRequest.getParameter().get(key));
            }

            if (serviceRequest.getMultipleFile() != null){
                for (String key : serviceRequest.getMultipleFile().keySet()) {
                    for (int i = 0; i < serviceRequest.getMultipleFile().get(key).size() ; i++) {
                        builder.addFormDataPart(key, serviceRequest.getMultipleFile().get(key).get(i).getName(), RequestBody.create(MEDIA_TYPE_JPG, serviceRequest.getMultipleFile().get(key).get(i)));
                    }
                }
            }else {
                for (String key : serviceRequest.getFiles().keySet()) {
                    builder.addFormDataPart(key, serviceRequest.getFiles().get(key).getName(), RequestBody.create(MEDIA_TYPE_JPG, serviceRequest.getFiles().get(key)));
                }
            }

            requestBody = builder.build();

            if (serviceRequest.getMultipleFile() != null){
                printLogRequest(url,
                        filter,
                        ServiceUtil.getParameter(serviceRequest.getParameter()) +"&"+ ServiceUtil.getMultipleParameterFile(serviceRequest.getMultipleFile()),
                        serviceRequest.getMethod(),
                        true);
            }else {
                printLogRequest(url,
                        filter,
                        ServiceUtil.getParameter(serviceRequest.getParameter()) +"&" + ServiceUtil.getFile(serviceRequest.getFiles()),
                        serviceRequest.getMethod(),
                        true);
            }


            request = new Request.Builder()
                    .url(url)
                    .post(requestBody)
                    .build();
        }

        ServiceUtil.client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                Log.i(":", "********************* Service Request **********************");
                Log.i("Filter", filter);
                Log.i("Message", e.getMessage());
                Log.i(":", "************************************************************");
                e.printStackTrace();
                ServiceResponse serviceResponse = new ServiceResponse();
                serviceResponse.setCode(10);
                serviceResponse.setMessage(TAG_TIMEOUT);
                sendBroadcastIntent(serviceResponse, filter);
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                int code = response.code();
                if (code == 500) {
                    ServiceResponse serviceResponse = new ServiceResponse();
                    serviceResponse.setCode(500);
                    serviceResponse.setMessage("Connection problem. Please check your internet connection.");
                    /*serviceResponse.setMessage("Internal Server Error");*/
                    printLogSuccess(filter, serviceResponse.getCode(), "");
                    sendBroadcastIntent(serviceResponse, filter);
                } else {
                    String content = response.body().string();
                    try {
                        ServiceResponse serviceResponse = new ServiceResponse();
                        JSONObject jsonObject = new JSONObject(content);
                        if (jsonObject.getString("status").equals("success")) {
                            serviceResponse.setCode(200);
                            serviceResponse.setMessage(jsonObject.getString("msg"));
                            serviceResponse.setContent(content);
                        } else {
                            serviceResponse.setCode(400);
                            serviceResponse.setMessage(jsonObject.getString("msg"));
                            serviceResponse.setContent(content);
                        }
                        printLogSuccess(filter, serviceResponse.getCode(), content);
                        sendBroadcastIntent(serviceResponse, filter);
                    } catch (Exception e) {
                        ServiceResponse serviceResponse = new ServiceResponse();
                        serviceResponse.setCode(400);
                        serviceResponse.setMessage("Unknown Error");
                        serviceResponse.setContent(content);
                        printLogSuccess(filter, serviceResponse.getCode(), content);
                        e.printStackTrace();
                        sendBroadcastIntent(serviceResponse, filter);
                    }
                }
            }
        });
    }

    private void printLogSuccess(String filter, int code, String content) {
        Log.i(":", "===================== Service Response ====================");
        Log.i("Filter", filter);
        Log.i("Code", code + "");
        Log.i("Content", content);
        Log.i(":", "===========================================================");
    }

    private void printLogRequest(String url, String filter, String method, String parameter, boolean multi) {
        Log.i(":", "********************* Service Request **********************");
        Log.i("URI", url);
        Log.i("Filter", filter);
        Log.i("Method", method);
        Log.i("Multypart", multi + "");
        Log.i("Parameter", parameter);
        Log.i(":", "************************************************************");
    }

    private void sendBroadcastIntent(ServiceResponse serviceResponse, String filter) {
        Intent broadcastIntent = new Intent();
        broadcastIntent.setAction(filter);
        broadcastIntent.putExtra(TAG_RESPONSE, serviceResponse);
        LocalBroadcastManager localBroadcastManager = LocalBroadcastManager.getInstance(this);
        localBroadcastManager.sendBroadcast(broadcastIntent);
        sendBroadcast(broadcastIntent);
    }
}
