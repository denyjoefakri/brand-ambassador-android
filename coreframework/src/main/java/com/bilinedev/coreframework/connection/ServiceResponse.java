package com.bilinedev.coreframework.connection;

import java.io.Serializable;

/**
 * Created by pristyanchandra on 10/14/16.
 */
public class ServiceResponse implements Serializable {
    private int code;
    private String content;
    private String message;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
