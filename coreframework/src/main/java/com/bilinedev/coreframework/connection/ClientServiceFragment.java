package com.bilinedev.coreframework.connection;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.Fragment;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by pristyanchandra on 10/14/16.
 */
public class ClientServiceFragment extends Fragment {

    public void sendGetRequest(String path, HashMap<String, String> parameter, String filter, String key){
        ServiceRequest serviceRequest = new ServiceRequest();
        serviceRequest.setPath(path);
        serviceRequest.setMethod(ClientService.METHOD_GET);
        serviceRequest.setParameter(parameter);
        serviceRequest.setFilter(filter);
        serviceRequest.setKey(key);
        serviceRequest.setHaveFile(false);

        Intent i = new Intent(getActivity(), ClientService.class);
        i.putExtra(ClientService.TAG_REQUEST, serviceRequest);
        getActivity().startService(i);
    }

    public void sendPostRequest(String path, HashMap<String, String> parameter, String filter, String key){
        ServiceRequest serviceRequest = new ServiceRequest();
        serviceRequest.setPath(path);
        serviceRequest.setMethod(ClientService.METHOD_POST);
        serviceRequest.setParameter(parameter);
        serviceRequest.setFilter(filter);
        serviceRequest.setKey(key);
        serviceRequest.setHaveFile(false);

        Intent i = new Intent(getActivity(), ClientService.class);
        i.putExtra(ClientService.TAG_REQUEST, serviceRequest);
        getActivity().startService(i);
    }

    public void sendMultiplePostRequest(String path, HashMap<String,String> singleParam, HashMap<String, ArrayList<String>> multipleParam, String filter, String key) {
        ServiceRequest serviceRequest = new ServiceRequest();
        serviceRequest.setPath(path);
        serviceRequest.setMethod(ClientService.METHOD_POST);
        serviceRequest.setParameter(singleParam);
        serviceRequest.setMultipleParameter(multipleParam);
        serviceRequest.setFilter(filter);
        serviceRequest.setKey(key);

        Intent i = new Intent(getActivity(), ClientService.class);
        i.putExtra(ClientService.TAG_REQUEST, serviceRequest);
        getActivity().startService(i);
    }

    public void sendPostRequestWithArrayParam(String path, HashMap<String, String> parameter, HashMap<String, ArrayList<String>> paramMultiple, String filter, String key){
        ServiceRequest serviceRequest = new ServiceRequest();
        serviceRequest.setPath(path);
        serviceRequest.setMethod(ClientService.METHOD_POST);
        serviceRequest.setParameter(parameter);
        serviceRequest.setFilter(filter);
        serviceRequest.setKey(key);
        if (paramMultiple !=null){
            serviceRequest.setMultipleParameter(paramMultiple);
        }
        serviceRequest.setHaveFile(false);

        Intent i = new Intent(getActivity(), ClientService.class);
        i.putExtra(ClientService.TAG_REQUEST, serviceRequest);
        getActivity().startService(i);
    }

    public BroadcastReceiver getReceiver() {
        return new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                ServiceResponse serviceResponse = (ServiceResponse) intent.getSerializableExtra(ClientService.TAG_RESPONSE);

                switch (serviceResponse.getCode()) {
                    case 10:
                        onTimeout(intent.getAction());
                        break;
                    case 200:
                    case 204:
                        successResponse(serviceResponse, intent.getAction());
                        break;
                    default:
                        if (serviceResponse.getMessage() == null || serviceResponse.getMessage().equals("null")) {
                            onTimeout(intent.getAction());
                        } else {
                            badResponse(serviceResponse, intent.getAction());
                        }
                        break;
                }
            }
        };
    }

    public void successResponse(ServiceResponse response, String filter) {}

    public void badResponse(ServiceResponse response, String filter) {}

    public void onTimeout(String filter) {}
}
