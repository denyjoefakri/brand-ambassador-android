package com.bilinedev.coreframework.connection;

import android.app.IntentService;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import org.json.JSONObject;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.HttpUrl;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * Created by pristyanchandra on 10/14/16.
 */
public class ClientServiceMultiple extends IntentService {

    public static final String TAG_RESPONSE = "serviceResponse";
    public static final String TAG_REQUEST = "serviceRequest";
    public static final String TAG_TIMEOUT = "Request Timeout";

    public static final String METHOD_GET = "GET";
    public static final String METHOD_POST = "POST";
    public static final MediaType TYPE_URL_ENCODED = MediaType.parse("application/x-www-form-urlencoded; charset=utf-8");
    private static final MediaType MEDIA_TYPE_JPG = MediaType.parse("image/jpg");

    public ClientServiceMultiple() {
        super("ClientService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        OkHttpClient client = new OkHttpClient.Builder()
                .readTimeout(5, TimeUnit.MINUTES)
                .writeTimeout(5, TimeUnit.MINUTES)
                .build();
        final ServiceRequest serviceRequest = (ServiceRequest) intent.getSerializableExtra(TAG_REQUEST);
        String url = serviceRequest.getPath() + serviceRequest.getKey();
        final String filter = serviceRequest.getFilter();
        String fullUrl;
        HttpUrl.Builder urlBuilder = HttpUrl.parse(url).newBuilder();
        Request request;
        RequestBody requestBody = null;

        if (serviceRequest.getMultipleFile() == null) {
            if (serviceRequest.getMethod().equals(METHOD_POST)) {
                requestBody = RequestBody.create(TYPE_URL_ENCODED, ServiceUtil.getMultipleParameter(serviceRequest.getMultipleParameter()));
                request = new Request.Builder()
                        .url(url)
                        .post(requestBody)
                        .build();
                printLogRequest(url,
                        filter,
                        serviceRequest.getMethod(),
                        ServiceUtil.getMultipleParameter(serviceRequest.getMultipleParameter()),
                        false);
            } else {
                if (serviceRequest.getMultipleParameter() != null) {
                    for (String key : serviceRequest.getMultipleParameter().keySet()) {
                        for (int i = 0; i < serviceRequest.getMultipleParameter().size(); i++) {
                            urlBuilder.addQueryParameter(key, serviceRequest.getMultipleParameter().get(key).get(i));
                        }
                    }
                }
                fullUrl = urlBuilder.build().toString();
                request = new Request.Builder()
                        .url(fullUrl)
                        .build();
                printLogRequest(fullUrl,
                        filter,
                        serviceRequest.getMethod(),
                        ServiceUtil.getMultipleParameter(serviceRequest.getMultipleParameter()),
                        false);
            }
        } else {
            MultipartBody.Builder builder = new MultipartBody.Builder();
            builder.setType(MultipartBody.FORM);

            for (String key : serviceRequest.getMultipleParameter().keySet()) {
                for (int i = 0; i < serviceRequest.getMultipleParameter().get(key).size(); i++) {
                    builder.addFormDataPart(key, serviceRequest.getMultipleParameter().get(key).get(i));
                }
            }

            for (String key : serviceRequest.getMultipleFile().keySet()) {
                for (int i = 0; i < serviceRequest.getMultipleFile().get(key).size() ; i++) {
                    builder.addFormDataPart(key, serviceRequest.getMultipleFile().get(key).get(i).getName(), RequestBody.create(MEDIA_TYPE_JPG, serviceRequest.getMultipleFile().get(key).get(i)));
                }
            }
            requestBody = builder.build();

            printLogRequest(url,
                    filter,
                    ServiceUtil.getMultipleParameter(serviceRequest.getMultipleParameter()),
                    serviceRequest.getMethod(),
                    true);
            request = new Request.Builder()
                    .url(url)
                    .post(requestBody)
                    .build();
        }

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                Log.i(":", "********************* Service Request **********************");
                Log.i("Filter", filter);
                Log.i("Message", e.getMessage());
                Log.i(":", "************************************************************");
                e.printStackTrace();
                ServiceResponse serviceResponse = new ServiceResponse();
                serviceResponse.setCode(10);
                serviceResponse.setMessage(TAG_TIMEOUT);
                sendBroadcastIntent(serviceResponse, filter);
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                int code = response.code();
                if (code == 500) {
                    ServiceResponse serviceResponse = new ServiceResponse();
                    serviceResponse.setCode(500);
                    serviceResponse.setMessage("Connection problem. Please check your internet connection.");
                    /*serviceResponse.setMessage("Internal Server Error");*/
                    printLogSuccess(filter, serviceResponse.getCode(), response.body().string());
                    sendBroadcastIntent(serviceResponse, filter);
                } else {
                    String content = response.body().string();
                    try {
                        ServiceResponse serviceResponse = new ServiceResponse();
                        JSONObject jsonObject = new JSONObject(content);
                        if (jsonObject.getString("status").equals("success")) {
                            serviceResponse.setCode(200);
                            serviceResponse.setMessage(response.message());
                            serviceResponse.setContent(content);
                        } else {
                            serviceResponse.setCode(400);
                            serviceResponse.setMessage(jsonObject.getString("error_message"));
                            serviceResponse.setContent(content);
                        }
                        printLogSuccess(filter, serviceResponse.getCode(), content);
                        sendBroadcastIntent(serviceResponse, filter);
                    } catch (Exception e) {
                        ServiceResponse serviceResponse = new ServiceResponse();
                        serviceResponse.setCode(400);
                        serviceResponse.setMessage("Unknown Error");
                        serviceResponse.setContent(content);
                        printLogSuccess(filter, serviceResponse.getCode(), content);
                        e.printStackTrace();
                        sendBroadcastIntent(serviceResponse, filter);
                    }
                }
            }
        });
    }

    private void printLogSuccess(String filter, int code, String content) {
        Log.i(":", "===================== Service Response ====================");
        Log.i("Filter", filter);
        Log.i("Code", code + "");
        Log.i("Content", content);
        Log.i(":", "===========================================================");
    }

    private void printLogRequest(String url, String filter, String method, String parameter, boolean multi) {
        Log.i(":", "********************* Service Request **********************");
        Log.i("URI", url);
        Log.i("Filter", filter);
        Log.i("Method", method);
        Log.i("Multypart", multi + "");
        Log.i("Parameter", parameter);
        Log.i(":", "************************************************************");
    }

    private void sendBroadcastIntent(ServiceResponse serviceResponse, String filter) {
        Intent broadcastIntent = new Intent();
        broadcastIntent.setAction(filter);
        broadcastIntent.putExtra(TAG_RESPONSE, serviceResponse);
        LocalBroadcastManager localBroadcastManager = LocalBroadcastManager.getInstance(this);
        localBroadcastManager.sendBroadcast(broadcastIntent);
        sendBroadcast(broadcastIntent);
    }
}
