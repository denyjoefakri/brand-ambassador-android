///*
//package com.bilinedev.coreframework.slidingdrawer;
//
//import android.content.Intent;
//import android.os.Bundle;
//import android.support.v4.app.Fragment;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//import android.widget.TextView;
//
//import com.bilinedev.new20fitpos.R;
//import com.bilinedev.new20fitpos.data.UserPreference;
//import com.bilinedev.new20fitpos.ui.activity.LoginActivity;
//import com.bilinedev.new20fitpos.ui.activity.MainActivity;
//import com.bilinedev.new20fitpos.ui.fragment.AppointmentFragment;
//import com.bilinedev.new20fitpos.ui.fragment.MemberFragment;
//import com.bilinedev.new20fitpos.ui.fragment.OrderFragment;
//import com.bilinedev.new20fitpos.ui.fragment.PettycashFragment;
//
//public class SlidingMenuFragment extends Fragment {
//
//	UserPreference userPreference;
//
//	public SlidingMenuFragment() {}
//
//	public static SlidingMenuFragment newInstance() {
//		return new SlidingMenuFragment();
//	}
//
//	public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {
//		View rootView = inflater.inflate(R.layout.navigation_drawer, parent, false);
//
//		userPreference = UserPreference.getInstance(getActivity());
//		TextView menuAppointment = (TextView) rootView.findViewById(R.id.menuAppointment);
//		TextView menuMember = (TextView) rootView.findViewById(R.id.menuMember);
//		TextView menuOrder = (TextView) rootView.findViewById(R.id.menuOrders);
//		TextView menuInvoice = (TextView) rootView.findViewById(R.id.menuInvoice);
//		TextView menuPettyCash = (TextView) rootView.findViewById(R.id.menuPettyCash);
//		TextView menuLogout = (TextView) rootView.findViewById(R.id.menuLogout);
//		TextView menuCsName = (TextView) rootView.findViewById(R.id.menuCsName);
//		TextView menuTrainer = (TextView) rootView.findViewById(R.id.menuTrainerStats);
//
//		menuCsName.setText(userPreference.getFullname());
//
//		menuAppointment.setOnClickListener(onClickListener);
//		menuMember.setOnClickListener(onClickListener);
//		menuOrder.setOnClickListener(onClickListener);
//		menuInvoice.setOnClickListener(onClickListener);
//		menuPettyCash.setOnClickListener(onClickListener);
//		menuLogout.setOnClickListener(onClickListener);
//		menuTrainer.setOnClickListener(onClickListener);
//		menuCsName.setOnClickListener(onClickListener);
//		return rootView;
//	}
//
//	View.OnClickListener onClickListener = new View.OnClickListener() {
//		@Override
//		public void onClick(View v) {
//			switch (v.getId()) {
//				case R.id.menuAppointment:
//					((MainActivity) getActivity()).toggleMenu(new AppointmentFragment(), "Appointment");
//					break;
//				case R.id.menuMember:
//					((MainActivity) getActivity()).toggleMenu(new MemberFragment(), "Members");
//					break;
//				case R.id.menuOrders:
//					((MainActivity) getActivity()).toggleMenu(new OrderFragment(), "Orders");
//					break;
//				case R.id.menuInvoice:
//
//					break;
//				case R.id.menuPettyCash:
//					((MainActivity) getActivity()).toggleMenu(new PettycashFragment(), "Petty Cash");
//					break;
//				case R.id.menuLogout:
//					userPreference.setAdminId(null);
//					userPreference.setEmail(null);
//					userPreference.setBranchTitle(null);
//					userPreference.setBranchId(null);
//					userPreference.setRole(null);
//					userPreference.setFullname(null);
//					Intent i = new Intent(getActivity(), LoginActivity.class);
//					i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
//					startActivity(i);
//					getActivity().finish();
//					break;
//				case R.id.menuCsName:
//
//					break;
//				case R.id.menuTrainerStats:
//
//					break;
//			}
//		}
//	};
//}
//*/
