package com.bilinedev.coreframework.model;

import java.io.Serializable;

/**
 * Created by pristyanchandra on 10/14/16.
 */

public class DoubleSpinnerModel implements Serializable {
    private String title;
    private String desc;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}
