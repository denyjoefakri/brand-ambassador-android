package com.bilinedev.coreframework.localdb;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteConstraintException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by pristyanchandra on 10/14/16.
 */
public class DBHelper extends SQLiteOpenHelper {

    /** dbName harus diakhiri dengan .db */
    public DBHelper(Context context, String dbName) {
        super(context, dbName, null, 1);
    }

    /** db.execSQL create table setiap onCreate */
    @Override
    public void onCreate(SQLiteDatabase db) {
        /*db.execSQL("create table " + TABLE_USER + "(" +
                COLUMN_USER_ID + " text primary key, " +
                COLUMN_USER_EMAIL + " text," +
                COLUMN_USER_NAME + " text," +
                COLUMN_USER_PHONE + " text)");*/
    }

    /** db.execSQL drop exist table setiap onUpgrade */
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        /*db.execSQL("DROP TABLE IF EXISTS " + TABLE_USER);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_SCHEDULE);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_ROOM);
        onCreate(db);*/
    }

    public void insertIntoTable(String tableName, ContentValues contentValues) throws SQLiteConstraintException {
        SQLiteDatabase db = this.getWritableDatabase();
        db.insert(tableName, null, contentValues);
    }

    public void dropTable(String tableName) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("delete from " + tableName);
    }

    public boolean udpateTable(String tableName, ContentValues contentValues, String keyCondition, String valueCondition) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.update(tableName, contentValues, "" + keyCondition + " = ? ", new String[]{valueCondition});
        return true;
    }

    public Cursor selectTable(String query) {
        SQLiteDatabase db = this.getReadableDatabase();
        return db.rawQuery(query, null);
    }

    /*public ArrayList<MemberModel> getAllMember() {
        ArrayList<MemberModel> array_list = new ArrayList<>();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res = db.rawQuery("select * from " + TABLE_USER, null);
        res.moveToFirst();
        while (!res.isAfterLast()) {
            MemberModel memberModel = new MemberModel();
            memberModel.setId(res.getString(res.getColumnIndex(COLUMN_USER_ID)));
            memberModel.setName(res.getString(res.getColumnIndex(COLUMN_USER_NAME)));
            memberModel.setEmail(res.getString(res.getColumnIndex(COLUMN_USER_EMAIL)));
            memberModel.setPhone(res.getString(res.getColumnIndex(COLUMN_USER_PHONE)));
            array_list.add(memberModel);
            res.moveToNext();
        }
        return array_list;
    }*/
}