package com.bilinedev.coreframework.ui.widget.spinner;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.bilinedev.coreframework.R;
import com.bilinedev.coreframework.model.DoubleSpinnerModel;

import java.util.ArrayList;

/**
 * Created by pristyanchandra on 10/14/16.
 */

public class DoubleSpinnerAdapter extends ArrayAdapter {

    Context context;
    ArrayList<DoubleSpinnerModel> item;
    int itemLayout;
    LayoutInflater inflater;

    public DoubleSpinnerAdapter(Context context, LayoutInflater inflater, ArrayList<DoubleSpinnerModel> item) {
        super(context, R.layout.item_single_spinner_adapter, item);
        this.context = context;
        this.inflater = inflater;
        this.itemLayout = R.layout.item_single_spinner_adapter;
        this.item = item;
    }

    public View getCustomView(int position, View convertView, ViewGroup parent) {
        View layout = inflater.inflate(R.layout.item_double_spinner_adapter, parent, false);
        TextView tvTitle = (TextView) layout.findViewById(R.id.item_spinner_text);
        TextView tvDesc = (TextView) layout.findViewById(R.id.item_spinner_text_second);
        tvTitle.setText(item.get(position).getTitle());
        tvDesc.setText(item.get(position).getDesc());
        return layout;
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        return getCustomView(position, convertView, parent);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        return getCustomView(position, convertView, parent);
    }
}