package com.bilinedev.coreframework.ui;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bilinedev.coreframework.R;
import com.bilinedev.coreframework.connection.ClientServiceActivity;
import com.bilinedev.coreframework.connection.ClientServiceFragment;
import com.bilinedev.coreframework.slidingdrawer.SlidingMenu;
import com.bilinedev.coreframework.util.DialogUtil;

/**
 * Created by pristyanchandra on 10/14/16.
 */

public class BaseActivity extends ClientServiceActivity {
    public SlidingMenu menu;
    public TextView toolbarTitle;
    public TextView toolbarLeft;
    public TextView toolbarRight;
    public TextView tabLeft;
    public TextView tabRight;
    public RelativeLayout toolbarCustom;
    public LinearLayout toolbarCustomTab;
    public ImageView toolbarIconApps;
    public ImageView toolbarIcon;
    public ImageView toolbarMenu;
    public ImageView toolbarMenu2;
    public ProgressDialog progressDialog;

    public void initCustomToolbar() {
        toolbarCustom = (RelativeLayout) findViewById(R.id.toolbar_custom);
        toolbarCustomTab = (LinearLayout) findViewById(R.id.toolbar_tab);
        toolbarTitle = (TextView) findViewById(R.id.toolbar_title);
        toolbarLeft = (TextView) findViewById(R.id.toolbar_left_text);
        toolbarRight = (TextView) findViewById(R.id.toolbar_right_text);
        tabLeft = (TextView) findViewById(R.id.txtTabLeft);
        tabRight = (TextView) findViewById(R.id.txtTabRight);
        toolbarIconApps = (ImageView) findViewById(R.id.toolbar_icon_apps);
        toolbarIcon = (ImageView) findViewById(R.id.toolbar_icon);
        toolbarMenu = (ImageView) findViewById(R.id.toolbar_menu);
    }

    public void setCustomNavigationDrawer(String title, int ic_drawer) {
        toolbarTitle.setText(title);
        toolbarIcon.setImageResource(ic_drawer);
        toolbarIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                menu.toggle();
            }
        });
    }

    public void setCustomNavigationBack(final Activity activity, String title, int ic_back) {
        toolbarTitle.setText(title);
        toolbarTitle.setTextColor(getResources().getColor(R.color.white));
        toolbarIcon.setImageResource(ic_back);
        toolbarIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                activity.finish();
            }
        });
    }

    public void setCustomNavigationBackWithIconApps(final Activity activity, int ic_iconApps, int ic_back) {
        toolbarIconApps.setImageResource(ic_iconApps);
        toolbarTitle.setTextColor(getResources().getColor(R.color.white));
        toolbarIcon.setImageResource(ic_back);
        toolbarIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                activity.finish();
            }
        });
    }

    public void setCustomNavigation(int ic_iconApps, int ic_iconMenu, int ic_iconMenu2, View.OnClickListener menuListener, View.OnClickListener menulistener2) {
        toolbarIconApps.setImageResource(ic_iconApps);
        toolbarIcon.setImageResource(ic_iconMenu);
        toolbarIcon.setPadding(20,20,20,20);
        toolbarIcon.setOnClickListener(menuListener);
        toolbarMenu.setImageResource(ic_iconMenu2);
        toolbarMenu.setPadding(30,30,30,30);
        toolbarMenu.setOnClickListener(menulistener2);
    }

    public void setCustomNavigationTab(int ic_iconMenu2,  int textLeft, int textRight, View.OnClickListener menulistener, View.OnClickListener tabListenerLeft, View.OnClickListener tabListenerRight) {
        toolbarCustomTab.setVisibility(View.VISIBLE);
        toolbarMenu.setImageResource(ic_iconMenu2);
        toolbarMenu.setOnClickListener(menulistener);
        tabLeft.setText(getResources().getString(textLeft));
        tabRight.setText(getResources().getString(textRight));
        tabLeft.setOnClickListener(tabListenerLeft);
        tabRight.setOnClickListener(tabListenerRight);

    }

    public void tabLeftSelect(){
        tabLeft.setBackgroundResource(R.drawable.bg_tab_select_left_nav);
        tabLeft.setTextColor(getResources().getColor(R.color.orange000));
        tabRight.setBackgroundResource(R.drawable.bg_tab_unselect_right_nav);
        tabRight.setTextColor(Color.WHITE);
        tabLeft.setHeight(90);
        tabRight.setHeight(80);
    }

    public void tabRightSelect(){
        tabRight.setBackgroundResource(R.drawable.bg_tab_select_right_nav);
        tabRight.setTextColor(getResources().getColor(R.color.orange000));
        tabLeft.setBackgroundResource(R.drawable.bg_tab_unselect_left_nav);
        tabLeft.setTextColor(Color.WHITE);
        tabRight.setHeight(90);
        tabLeft.setHeight(80);
    }

    public void setCustomNavigationBackRight(final Activity activity, String title, int ic_back, View.OnClickListener menuListener) {
        toolbarRight.setVisibility(View.VISIBLE);
        toolbarRight.setText(title);
        toolbarRight.setTextColor(getResources().getColor(R.color.white));
        toolbarRight.setOnClickListener(menuListener);
        toolbarIcon.setImageResource(ic_back);
        toolbarIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                activity.finish();
            }
        });
    }

    public void setCustomNavigation(final Activity activity, String title, String leftTitle, String rightTitle, View.OnClickListener rightListener) {
        toolbarTitle.setText(title);
        toolbarTitle.setTextColor(getResources().getColor(R.color.white));
        toolbarRight.setVisibility(View.VISIBLE);
        toolbarRight.setText(rightTitle);
        toolbarRight.setTextColor(getResources().getColor(R.color.white));
        toolbarRight.setOnClickListener(rightListener);
        toolbarLeft.setVisibility(View.VISIBLE);
        toolbarLeft.setText(leftTitle);
        toolbarLeft.setTextColor(getResources().getColor(R.color.white));
        toolbarLeft.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activity.finish();
            }
        });
    }

    public void setCustomNavigation2(final Activity activity, String title, int ic_back, String rightTitle, View.OnClickListener rightListener) {
        toolbarTitle.setText(title);
        toolbarTitle.setTextColor(getResources().getColor(R.color.white));
        toolbarRight.setVisibility(View.VISIBLE);
        toolbarRight.setText(rightTitle);
        toolbarRight.setTextColor(getResources().getColor(R.color.white));
        toolbarRight.setOnClickListener(rightListener);
        toolbarIcon.setImageResource(ic_back);
        toolbarIcon.setPadding(30,30,30,30);
        toolbarIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                activity.finish();
            }
        });
    }

    public void setCustomNavigationTitle(String title, int i) {
        toolbarTitle.setText(title);
        toolbarTitle.setTextColor(getResources().getColor(i));
    }

    public void initComponents(){}

    public void setIconCustomMenuOption(int imgRes) {
        toolbarMenu.setImageResource(imgRes);
    }

    public void setCustomMenuOption(int imgRes, View.OnClickListener menuListener) {
        toolbarMenu.setImageResource(imgRes);
        toolbarMenu.setOnClickListener(menuListener);
    }

    public void showProgressMessage(Context context, String message, boolean cancelable) {
        progressDialog = DialogUtil.createProgressDialog(context, message, cancelable);
        progressDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
            }
        });
        progressDialog.show();
    }

    public void dismissProgressMessage() {
        if (progressDialog != null) {
            progressDialog.dismiss();
        }
    }
}
