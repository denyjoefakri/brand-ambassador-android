package com.bilinedev.coreframework.ui.dialog;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.TextView;

import com.bilinedev.coreframework.R;

/**
 * Created by Chandra on 10/14/16.
 */
public class ConfirmationDialog extends DialogFragment {

    private OnMessageClosed messageClosed;

    public static ConfirmationDialog newIntance(String message) {
        ConfirmationDialog messageDialog = new ConfirmationDialog();
        Bundle bundle = new Bundle();
        bundle.putCharSequence("message", message);
        messageDialog.setArguments(bundle);
        return messageDialog;
    }

    public void setOnMessageClosed(OnMessageClosed messageClosed) {
        this.messageClosed = messageClosed;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.dialog_confirmation, container, false);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NO_TITLE, R.style.CustomDialog);
    }

    @Override
    public void onViewCreated(View v, @Nullable Bundle savedInstanceState) {
        getDialog().requestWindowFeature(Window.FEATURE_NO_TITLE);
        setStyle(STYLE_NO_FRAME, 0);
        TextView txtMessage = (TextView) v.findViewById(R.id.dialog_message_confirmation);
        txtMessage.setText(getArguments().getCharSequence("message"));

        TextView btnYes = (TextView) v.findViewById(R.id.dialog_yes);
        btnYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (messageClosed != null) {
                    messageClosed.onProcess();
                }
                dismiss();
            }
        });

        TextView btnCancel = (TextView) v.findViewById(R.id.dialog_no);
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (messageClosed != null) {
                    messageClosed.onCancel();
                }
                dismiss();
            }
        });

    }

    public interface OnMessageClosed {
        public void onProcess();
        public void onCancel();
    }
}