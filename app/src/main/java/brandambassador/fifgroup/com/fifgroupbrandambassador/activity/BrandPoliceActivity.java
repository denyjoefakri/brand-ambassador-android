package brandambassador.fifgroup.com.fifgroupbrandambassador.activity;

import android.content.BroadcastReceiver;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.FileProvider;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;

import com.bilinedev.coreframework.connection.ServiceResponse;
import com.bilinedev.coreframework.ui.dialog.MessageDialog;
import com.bilinedev.coreframework.util.DateUtil;
import com.bilinedev.coreframework.util.DialogUtil;
import com.bilinedev.coreframework.util.ImageUtil;
import com.bilinedev.coreframework.util.PermissionUtil;
import com.bumptech.glide.Glide;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;

import brandambassador.fifgroup.com.fifgroupbrandambassador.BaseActivity;
import brandambassador.fifgroup.com.fifgroupbrandambassador.R;
import brandambassador.fifgroup.com.fifgroupbrandambassador.connection.Filter;
import brandambassador.fifgroup.com.fifgroupbrandambassador.connection.Path;
import brandambassador.fifgroup.com.fifgroupbrandambassador.connection.remote.FileDownloader;
import brandambassador.fifgroup.com.fifgroupbrandambassador.util.Konstan;
import brandambassador.fifgroup.com.fifgroupbrandambassador.util.UserPreference;
import brandambassador.fifgroup.com.fifgroupbrandambassador.util.Util;
import butterknife.Bind;
import butterknife.OnClick;

/**
 * Created by air-water on 2/5/17.
 */

public class BrandPoliceActivity extends BaseActivity {

    UserPreference userPreference;
    BroadcastReceiver receiver = getReceiver();

    ImageUtil imageUtil;
    File fileWrongImage;
    File fileNewImage;

    @Bind(R.id.view_place_image_wrong) View viewPlaceImageWrong;
    @Bind(R.id.place_image_wrong) ImageView placeImageWrong;
    @Bind(R.id.delete_image_wrong) ImageView deletePlaceImageWrong;
    @Bind(R.id.view_place_image_new) View viewPlaceImageNew;
    @Bind(R.id.place_image_new) ImageView placeImageNew;
    @Bind(R.id.delete_image_new) ImageView deletePlaceImageNew;
    @Bind(R.id.uploadWrongImage) ImageButton uploadWrongImage;
    @Bind(R.id.uploadNewImage) ImageButton uploadNewImage;
    @Bind(R.id.edDescription) EditText edDescription;
    @Bind(R.id.mainImage) ImageView imageView;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setNavigationBackText(this, getString(R.string.t_brand_police));
        imageUtil = new ImageUtil(this);
        userPreference = UserPreference.getInstance(this);
        Util.setImage(this, R.drawable.brand_police_pic, imageView);

        imageUtil.setFolderName("Ambassador_App");
        imageUtil.setFileName("APP_" + DateUtil.getCurrentDateTimeForPhoto());


    }

    @Override
    protected int getResourceLayout() {
        return R.layout.activity_brand_police;
    }


    @OnClick (R.id.uploadWrongImage)
    public void uploadWrongImage(){
        if (PermissionUtil.isCameraGranted(this, this, getSupportFragmentManager(), 0)) {
            imageUtil.openImageChooser(this, uploadWrongImage, 1);
        }
    }

    @OnClick (R.id.uploadNewImage)
    public void uploadNewImage(){
        if (PermissionUtil.isCameraGranted(this, this, getSupportFragmentManager(), 1)) {
            imageUtil.openImageChooser(this, uploadNewImage, 2);
        }
    }

    @OnClick(R.id.btnSubmit)
    public void submit(){
        if (fileNewImage != null && fileWrongImage != null){
            HashMap<String, File> mapFile = new HashMap<>();
            mapFile.put("image_wrong", fileWrongImage);
            mapFile.put("image_right", fileNewImage);

            HashMap<String, String> mapString = new HashMap<>();
            mapString.put("employee_id", userPreference.getUserId());
            mapString.put("description", edDescription.getText().toString());

            postBrandPolice(mapString, mapFile);
            showProgressMessage(BrandPoliceActivity.this, getString(R.string.p_please_wait), false);
        } else {
            DialogUtil.messageDialog(getSupportFragmentManager(), getString(R.string.msg_image));
        }
    }

    @OnClick(R.id.btnGuide)
    public void guide(){
        if (userPreference.getDownloading()){
            pdf();
        } else {
            if (PermissionUtil.isStorageGranted(this, this, getSupportFragmentManager(), 2)){
                new DownloadFile().execute(Path.PDF, Konstan.PDF_NAME);
            }
        }
    }


    @Override
    protected void onResume() {
        super.onResume();
        registerReceiver(receiver, new IntentFilter(Filter.POST_BRAND_POLICE));
    }

    @Override
    protected void onStop() {
        super.onStop();
        unregisterReceiver(receiver);
    }

    @OnClick(R.id.delete_image_wrong)
    public void deleteImageWrong(){
        fileNewImage = null;
        uploadWrongImage.setVisibility(View.VISIBLE);
        viewPlaceImageWrong.setVisibility(View.GONE);
    }

    @OnClick(R.id.delete_image_new)
    public void deleteImageNew(){
        fileWrongImage = null;
        uploadNewImage.setVisibility(View.VISIBLE);
        viewPlaceImageNew.setVisibility(View.GONE);
    }

    @Override
    public void successResponse(ServiceResponse response, String filter) {
        super.successResponse(response, filter);
        dismissProgressMessage();
        try {
            JSONObject object = new JSONObject(response.getContent());
            JSONObject objectData = object.getJSONObject("data");
            DialogUtil.messageDialog(getSupportFragmentManager(), response.getMessage()).setOnMessageClosed(new MessageDialog.OnMessageClosed() {
                @Override
                public void onClosed() {
                    finish();
                }
            });
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void badResponse(ServiceResponse response, String filter) {
        super.badResponse(response, filter);
        dismissProgressMessage();
        DialogUtil.messageDialog(getSupportFragmentManager(), response.getMessage());
    }

    @Override
    public void onTimeout(String filter) {
        super.onTimeout(filter);
        dismissProgressMessage();
        DialogUtil.messageDialog(getSupportFragmentManager(), getString(R.string.msg_rto));
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1){
            File file = imageUtil.onHandleImage(requestCode, resultCode, data);
            if (file != null){
                fileWrongImage = ImageUtil.compressImage(this, file);
                uploadWrongImage.setVisibility(View.GONE);
                viewPlaceImageWrong.setVisibility(View.VISIBLE);
                Glide.with(this).load(fileWrongImage).into(placeImageWrong);
            }
        } else {
            File file = imageUtil.onHandleImage(requestCode, resultCode, data);
            if (file != null){
                Log.e("this 2", "tidak null");
                fileNewImage = ImageUtil.compressImage(this, file);
                uploadNewImage.setVisibility(View.GONE);
                viewPlaceImageNew.setVisibility(View.VISIBLE);
                Glide.with(this).load(fileNewImage).into(placeImageNew);
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (grantResults.length > 0) {
            Log.e("request code", ""+requestCode);
            if (requestCode == 0){
                imageUtil.openImageChooser(this, uploadWrongImage, 1);
            } else if (requestCode == 1){
                imageUtil.openImageChooser(this, uploadNewImage, 2);
            } else {
                new DownloadFile().execute(Path.PDF, Konstan.PDF_NAME);
            }
        } else {
            Util.initToast(this, getString(R.string.p_permissions_denied), Konstan.Toast_LENGTH_SHORT);
        }
    }

    private class DownloadFile extends AsyncTask<String, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showProgressMessage(BrandPoliceActivity.this, getString(R.string.p_please_wait), false);
        }

        @Override
        protected Void doInBackground(String... strings) {
            String fileUrl = strings[0];
            String fileName = strings[1];
            String extStorageDirectory = Environment.getExternalStorageDirectory().toString();
            File folder = new File(extStorageDirectory, "/" + Konstan.DOCUMENT_FOLDER);
            folder.mkdir();

            File pdfFile = new File(folder, fileName);

            try{
                pdfFile.createNewFile();
            }catch (IOException e){
                e.printStackTrace();
            }
            FileDownloader.downloadFile(fileUrl, pdfFile);
            Log.e("path", pdfFile.getAbsolutePath());
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            Log.e("end", "process");
            dismissProgressMessage();
            userPreference.setDownloading(true);
            pdf();
        }
    }

    public void pdf(){
        File pdfFile = new File(Environment.getExternalStorageDirectory() + "/" + Konstan.DOCUMENT_FOLDER + "/" + Konstan.PDF_NAME);

        Uri photoURI = FileProvider.getUriForFile(this, getApplicationContext().getPackageName() + ".provider", pdfFile);
        Log.e("uri", photoURI.getPath());
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setDataAndType(photoURI, "application/pdf");
        //intent.setDataAndType(Uri.fromFile(pdfFile), "application/pdf");
        intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
        intent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        startActivity(intent);
    }

    /*public void pdf(){
        File pdfFile = new File(Environment.getExternalStorageDirectory() + "/brandAmbassador/" + "brand.pdf");
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setDataAndType(Uri.fromFile(pdfFile), "application/pdf");
        intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
        startActivity(intent);
    }*/

}
