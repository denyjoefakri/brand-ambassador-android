package brandambassador.fifgroup.com.fifgroupbrandambassador.activity;

import android.content.BroadcastReceiver;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.bilinedev.coreframework.connection.ServiceResponse;
import com.bilinedev.coreframework.ui.dialog.MessageDialog;
import com.bilinedev.coreframework.util.DialogUtil;
import com.bumptech.glide.Glide;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import brandambassador.fifgroup.com.fifgroupbrandambassador.BaseActivity;
import brandambassador.fifgroup.com.fifgroupbrandambassador.R;
import brandambassador.fifgroup.com.fifgroupbrandambassador.adapter.BrandChallengeAdapter;
import brandambassador.fifgroup.com.fifgroupbrandambassador.connection.Filter;
import brandambassador.fifgroup.com.fifgroupbrandambassador.dialog.MessageDialogAnswer;
import brandambassador.fifgroup.com.fifgroupbrandambassador.model.BrandChallengeAnswerModel;
import brandambassador.fifgroup.com.fifgroupbrandambassador.model.Dummy;
import brandambassador.fifgroup.com.fifgroupbrandambassador.util.DialogUtilCustom;
import brandambassador.fifgroup.com.fifgroupbrandambassador.util.UserPreference;
import butterknife.Bind;
import butterknife.OnClick;

/**
 * Created by air-water on 2/5/17.
 */

public class BrandChallengeActivity extends BaseActivity {

    UserPreference userPreference;
    BroadcastReceiver receiver = getReceiver();

    BrandChallengeAdapter adapter;
    @Bind(R.id.rvListRadio) RecyclerView rvList;
    @Bind(R.id.pbList) ProgressBar pbList;
    @Bind(R.id.txtQuestion) TextView txtQuestion;
    @Bind(R.id.view_answer_radio) View view_answer_radio;
    @Bind(R.id.view_answer_image) View view_answer_image;
    @Bind(R.id.view_answer_intro) View view_answer_intro;
    @Bind(R.id.view_no_data) View view_no_data;
    @Bind(R.id.message) TextView txtMessage;
    @Bind(R.id.btnSubmitAnswer) Button btnSubmitAnswer;

    @Bind(R.id.imgLeft) ImageView imgLeft;
    @Bind(R.id.imgRight) ImageView imgRight;
    @Bind(R.id.rgAnswer) RadioGroup rgAnswer;
    @Bind(R.id.rbOne) RadioButton rbOne;
    @Bind(R.id.rbTwo) RadioButton rbTwo;
    RadioButton rB;

    String id = "";
    String bc_answer_alphabet = "";
    boolean typeText = false, typeImage = false;
    boolean once = false;
    boolean finished = false;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setNavigationBackText(this, getString(R.string.t_brand_challenge));
        userPreference = UserPreference.getInstance(this);
        initComponents();
    }

    @Override
    protected int getResourceLayout() {
        return R.layout.activity_brand_challenge;
    }

    @Override
    public void initComponents() {
        super.initComponents();
        adapter = new BrandChallengeAdapter(this);
        rvList.setLayoutManager(new LinearLayoutManager(this));
        rvList.setHasFixedSize(true);
        rvList.setAdapter(adapter);

        rgAnswer.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                typeImage = true;
            }
        });

        txtQuestion.setText(getString(R.string.msg_intro_answer));
        view_answer_intro.setVisibility(View.VISIBLE);
        btnSubmitAnswer.setText(getString(R.string.bt_ikuti_kuisnya));
    }

    public void getQuestion(){
        HashMap<String, String> map = new HashMap<>();
        map.put("id", userPreference.getUserId());
        getBrandChallenge(map);
        pbList.setVisibility(View.VISIBLE);
    }
    /*public void dummy(){
        ServiceResponse serviceResponse = new ServiceResponse();
        serviceResponse.setContent(Dummy.brandChallenge);
        serviceResponse.setCode(200);
        successResponse(serviceResponse, Filter.GET_BRAND_CHALLENGE);
    }*/


    @OnClick(R.id.btnSubmitAnswer)
    public void submit(){
        if (!once){
            txtQuestion.setText("");
            view_answer_intro.setVisibility(View.GONE);
            btnSubmitAnswer.setText(getString(R.string.bt_submit_answer));
            getQuestion();
            once = true;
            return;
        }

        if (typeText){
            HashMap<String, String> map = new HashMap<>();
            map.put("challenge_id", id);
            map.put("answer", bc_answer_alphabet);
            map.put("employee_id", userPreference.getUserId());
            postBrandChallenge(map);
            showProgressMessage(this, getString(R.string.p_please_wait), false);
        } else if (typeImage){
            int selectedId = rgAnswer.getCheckedRadioButtonId();
            rB = (RadioButton)findViewById(selectedId);
            HashMap<String, String> map = new HashMap<>();
            map.put("challenge_id", id);
            map.put("answer", rB.getText().toString());
            map.put("employee_id", userPreference.getUserId());
            postBrandChallenge(map);
            showProgressMessage(this, getString(R.string.p_please_wait), false);
        } else if (finished){
            finish();
        } else {
            DialogUtil.messageDialog(getSupportFragmentManager(), getString(R.string.msg_answer));
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        registerReceiver(receiver, new IntentFilter(Filter.GET_BRAND_CHALLENGE));
        registerReceiver(receiver, new IntentFilter(Filter.POST_BRAND_CHALLENGE));
    }

    @Override
    protected void onStop() {
        super.onStop();
        unregisterReceiver(receiver);
    }

    @Override
    public void successResponse(ServiceResponse response, String filter) {
        super.successResponse(response, filter);
        dismissProgressMessage();
        pbList.setVisibility(View.GONE);
        switch (filter){
            case Filter.GET_BRAND_CHALLENGE:
                try {
                    JSONObject object = new JSONObject(response.getContent());
                    if (!object.getString("data").equals("null")){
                        JSONObject objectData = object.getJSONObject("data");
                        txtQuestion.setText(objectData.getString("question"));
                        id = objectData.getString("id");
                        JSONArray answerArray = objectData.getJSONArray("answer");
                        ArrayList<BrandChallengeAnswerModel> brandChallengeAnswerModels = new ArrayList<>();
                        if (objectData.getString("type").equals("text")){
                            view_answer_radio.setVisibility(View.VISIBLE);
                            view_answer_image.setVisibility(View.GONE);
                            for (int i = 0; i < answerArray.length(); i++) {
                                JSONObject answerObject = answerArray.getJSONObject(i);
                                BrandChallengeAnswerModel model = new BrandChallengeAnswerModel();
                                model.setId(answerObject.getString("id"));
                                model.setBcId(answerObject.getString("bc_id"));
                                model.setBcAnswer(answerObject.getString("bc_answer"));
                                model.setBcAnswerAlphabet(answerObject.getString("bc_answer_alphabet"));
                                brandChallengeAnswerModels.add(model);
                            }
                            adapter.update(brandChallengeAnswerModels);
                            adapter.setBrandListener(listener);
                        } else {
                            bc_answer_alphabet = "";
                            view_answer_image.setVisibility(View.VISIBLE);
                            view_answer_radio.setVisibility(View.GONE);
                            JSONObject object1 = answerArray.getJSONObject(0);
                            JSONObject object2 = answerArray.getJSONObject(1);
                            Glide.with(BrandChallengeActivity.this).load(object1.getString("bc_answer")).error(R.drawable.image_square).into(imgLeft);
                            Glide.with(BrandChallengeActivity.this).load(object2.getString("bc_answer")).error(R.drawable.image_square).into(imgRight);
                            rbOne.setText(object1.getString("bc_answer_alphabet"));
                            rbTwo.setText(object2.getString("bc_answer_alphabet"));
                        }
                    } else {
                        finished = true;
                        typeText = false;
                        typeImage = false;
                        btnSubmitAnswer.setText(getString(R.string.msg_back));
                        view_no_data.setVisibility(View.VISIBLE);
                        txtMessage.setText(getString(R.string.msg_brand_challenge_finished));
                        txtMessage.setTextColor(getResources().getColor(R.color.green700));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                break;
            case Filter.POST_BRAND_CHALLENGE:
                txtQuestion.setText("");
                typeText = false;
                typeImage = false;
                view_answer_image.setVisibility(View.GONE);
                view_answer_radio.setVisibility(View.GONE);
                DialogUtilCustom.messageDialog(getSupportFragmentManager(), response.getMessage()).setOnMessageClosed(new MessageDialogAnswer.OnMessageClosed() {
                    @Override
                    public void onClosed() {
                        rgAnswer.clearCheck();
                        adapter.notifyDataSetChanged();
                        getQuestion();
                    }
                });
                break;
        }
    }

    @Override
    public void badResponse(ServiceResponse response, String filter) {
        super.badResponse(response, filter);
        dismissProgressMessage();
        pbList.setVisibility(View.GONE);
        txtQuestion.setText("");
        view_answer_image.setVisibility(View.GONE);
        view_answer_radio.setVisibility(View.GONE);
        DialogUtil.messageDialog(getSupportFragmentManager(), response.getMessage());
    }

    @Override
    public void onTimeout(String filter) {
        super.onTimeout(filter);
        dismissProgressMessage();
        pbList.setVisibility(View.GONE);
        txtQuestion.setText("");
        view_answer_image.setVisibility(View.GONE);
        view_answer_radio.setVisibility(View.GONE);
        DialogUtil.messageDialog(getSupportFragmentManager(), getString(R.string.msg_rto));
    }

    BrandChallengeAdapter.BrandListener listener = new BrandChallengeAdapter.BrandListener() {
        @Override
        public void onClickRadio(BrandChallengeAnswerModel brandChallengeAnswerModel) {
            Log.e("data", brandChallengeAnswerModel.getBcAnswerAlphabet());
            typeText = true;
            bc_answer_alphabet = brandChallengeAnswerModel.getBcAnswerAlphabet();
        }
    };

}
