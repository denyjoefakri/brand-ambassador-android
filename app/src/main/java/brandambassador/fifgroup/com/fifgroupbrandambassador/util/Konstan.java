package brandambassador.fifgroup.com.fifgroupbrandambassador.util;

/**
 * Created by air-water on 1/4/17.
 */

public class Konstan {
    /**APP*/
    public static final String APP_NAME = "SurveyApp";
    public static final String APP_FOTO_FILE = "surveyapp_";
    public static final String MESSAGE = "messagge";


    /**PDF*/
    public static final String PDF_NAME = "brand.pdf";
    public static final String DOCUMENT_FOLDER = "brandAmbassadorDocument";

    /** UTIL */
    public static final int Toast_LENGTH_SHORT = 0;
    public static final int Toast_LENGTH_LONG = 1;
    public static final String TRANSISI = "transisi";

}
