package brandambassador.fifgroup.com.fifgroupbrandambassador.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;

import java.util.ArrayList;

import brandambassador.fifgroup.com.fifgroupbrandambassador.R;
import brandambassador.fifgroup.com.fifgroupbrandambassador.model.BrandChallengeAnswerModel;
import brandambassador.fifgroup.com.fifgroupbrandambassador.model.HistoryModel;
import brandambassador.fifgroup.com.fifgroupbrandambassador.util.Util;
import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by pristyanchandra on 1/25/17.
 */

public class HistoryAdapter extends RecyclerView.Adapter {

    private ArrayList<HistoryModel> historyModels;
    HistoryListener historyListener;
    Context context;

    public HistoryAdapter(Context context) {
        this.context = context;
        historyModels = new ArrayList<>();
    }

    public void update(ArrayList<HistoryModel> historyModels) {
        this.historyModels = historyModels;
        notifyDataSetChanged();
    }

    public void setHistoryListener(HistoryListener historyListener){
        this.historyListener = historyListener;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_history, parent, false);
        return new Holder(v);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ((Holder) holder).setValue(historyModels.get(position));
    }

    @Override
    public int getItemCount() {
        return historyModels.size();
    }

    public class Holder extends RecyclerView.ViewHolder {
        @Bind(R.id.imgBrand) ImageView imgBrand;
        @Bind(R.id.imgBrandDesignCenter) ImageView imgBrandDesignCenter;
        @Bind(R.id.txtType) TextView txtType;
        @Bind(R.id.txtDate) TextView txtDate;
        @Bind(R.id.txtHistory) TextView txtHistory;
        @Bind(R.id.txtStatus) TextView txtStatus;

        Holder(View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
        }

        private void setValue(final HistoryModel historyModel) {
            if (historyModel.getTypeBrands().equals("brand_challenge")){
                txtType.setVisibility(View.GONE);
                imgBrandDesignCenter.setVisibility(View.GONE);
                imgBrand.setVisibility(View.VISIBLE);
                imgBrand.setImageResource(R.drawable.brand_challenge);
            } else if (historyModel.getTypeBrands().equals("brand_detective")){
                txtType.setVisibility(View.GONE);
                imgBrandDesignCenter.setVisibility(View.GONE);
                imgBrand.setVisibility(View.VISIBLE);
                imgBrand.setImageResource(R.drawable.brand_detective);
            } else if (historyModel.getTypeBrands().equals("brand_police")){
                txtType.setVisibility(View.GONE);
                imgBrandDesignCenter.setVisibility(View.GONE);
                imgBrand.setVisibility(View.VISIBLE);
                imgBrand.setImageResource(R.drawable.brand_police);
            } else if (historyModel.getTypeBrands().equals("request_desain") || historyModel.getTypeBrands().equals("konsultasi")){
                txtType.setVisibility(View.GONE);
                imgBrand.setVisibility(View.GONE);
                imgBrandDesignCenter.setVisibility(View.VISIBLE);
                imgBrandDesignCenter.setImageResource(R.drawable.design_center_bg);
            } else {
                imgBrand.setVisibility(View.GONE);
                imgBrandDesignCenter.setVisibility(View.GONE);
                txtType.setVisibility(View.VISIBLE);
            }


            if (historyModel.getTitle().contains("submit")){
                txtStatus.setVisibility(View.GONE);
            } else if (historyModel.getTitle().contains("approved")){
                txtStatus.setVisibility(View.VISIBLE);
                txtStatus.setText("APPROVED");
                txtStatus.setTextColor(context.getResources().getColor(R.color.green600));
            } else if (historyModel.getTitle().contains("rejected")){
                txtStatus.setVisibility(View.VISIBLE);
                txtStatus.setText("REJECTED");
                txtStatus.setTextColor(context.getResources().getColor(R.color.red600));
            }

            String[] getDate = historyModel.getDate().split(" ");
            String[] date = getDate[0].split("-");

            txtDate.setText(date[2] + " " + Util.getMonth(Integer.parseInt(date[1])) + " " + date[0]);
            txtHistory.setText(historyModel.getTitle());


            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //historyListener.onClick(historyModel);
                }
            });
        }
    }

    public interface HistoryListener{
        void onClick(HistoryModel historyModel);
    }

}
