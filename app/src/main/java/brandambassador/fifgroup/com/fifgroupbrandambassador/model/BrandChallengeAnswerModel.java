package brandambassador.fifgroup.com.fifgroupbrandambassador.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by air-water on 2/13/17.
 */

public class BrandChallengeAnswerModel {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("bc_id")
    @Expose
    private String bcId;
    @SerializedName("bc_answer")
    @Expose
    private String bcAnswer;
    @SerializedName("bc_answer_alphabet")
    @Expose
    private String bcAnswerAlphabet;
    @SerializedName("created")
    @Expose
    private String created;
    @SerializedName("modified")
    @Expose
    private Object modified;
    @SerializedName("deleted")
    @Expose
    private String deleted;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getBcId() {
        return bcId;
    }

    public void setBcId(String bcId) {
        this.bcId = bcId;
    }

    public String getBcAnswer() {
        return bcAnswer;
    }

    public void setBcAnswer(String bcAnswer) {
        this.bcAnswer = bcAnswer;
    }

    public String getBcAnswerAlphabet() {
        return bcAnswerAlphabet;
    }

    public void setBcAnswerAlphabet(String bcAnswerAlphabet) {
        this.bcAnswerAlphabet = bcAnswerAlphabet;
    }

    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }

    public Object getModified() {
        return modified;
    }

    public void setModified(Object modified) {
        this.modified = modified;
    }

    public String getDeleted() {
        return deleted;
    }

    public void setDeleted(String deleted) {
        this.deleted = deleted;
    }

}
