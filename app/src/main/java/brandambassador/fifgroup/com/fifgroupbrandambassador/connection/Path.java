package brandambassador.fifgroup.com.fifgroupbrandambassador.connection;


/**
 * Created by telolet on 10/18/16.
 */

public class Path {

    /**Login*/
    public static final String REGISTER = "register";
    public static final String LOGIN = "login";
    public static final String FORGOT_PASSWORD = "forgot-password";
    public static final String PROFILE = "profile";

    public static final String NOTIFICATION = "notification";
    public static final String HISTORY = "history";
    public static final String REDEEM = "redeem";
    public static final String LIST_REDEEM = "redeem/list";

    public static final String PDF = "http://ba-dev.arifchasan.com/uploads/petunjuk_teknis.pdf";
    public static final String IMAGE_REDEEM = "http://bafifgroup2017.com/uploads/redeem.jpg";

    public static final String POST_BRAND_DETECTIVE = "brand-detective";
    public static final String GET_BRAND_CHALLENGE = "brand-challenge";
    public static final String POST_BRAND_CHALLENGE = "brand-challenge";
    public static final String POST_BRAND_POLICE = "brand-police";

}

