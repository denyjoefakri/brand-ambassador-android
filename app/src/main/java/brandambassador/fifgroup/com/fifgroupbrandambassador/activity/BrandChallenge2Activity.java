package brandambassador.fifgroup.com.fifgroupbrandambassador.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import brandambassador.fifgroup.com.fifgroupbrandambassador.BaseActivity;
import brandambassador.fifgroup.com.fifgroupbrandambassador.R;
import butterknife.Bind;
import butterknife.OnClick;

/**
 * Created by air-water on 2/5/17.
 */

public class BrandChallenge2Activity extends BaseActivity {

    @Bind(R.id.txtQuestion) TextView textView;
    @Bind(R.id.pbList) ProgressBar pbList;
    @Bind(R.id.imgLeft) ImageView imgLeft;
    @Bind(R.id.imgRight) ImageView imgRight;
    @Bind(R.id.rgAnswer) RadioGroup rgAnswer;
    @Bind(R.id.rbOne) RadioButton rbOne;
    @Bind(R.id.rbTwo) RadioButton rbTwo;
    RadioButton rB;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setNavigationBackText(this, getString(R.string.t_brand_challenge));
        pbList.setVisibility(View.GONE);
    }

    @Override
    protected int getResourceLayout() {
        return R.layout.activity_brand_challenge_2;
    }

    @OnClick(R.id.btnSubmitAnswer)
    public void answer(){
        int selectedId = rgAnswer.getCheckedRadioButtonId();
        rB = (RadioButton)findViewById(selectedId);
        Toast.makeText(BrandChallenge2Activity.this, rB.getText(),Toast.LENGTH_SHORT).show();
    }

}
