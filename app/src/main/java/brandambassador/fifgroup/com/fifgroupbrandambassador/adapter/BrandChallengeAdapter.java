package brandambassador.fifgroup.com.fifgroupbrandambassador.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.RadioButton;
import android.widget.TextView;

import com.bilinedev.coreframework.util.NumberUtil;

import java.util.ArrayList;

import brandambassador.fifgroup.com.fifgroupbrandambassador.R;
import brandambassador.fifgroup.com.fifgroupbrandambassador.activity.BrandChallengeActivity;
import brandambassador.fifgroup.com.fifgroupbrandambassador.model.BrandChallengeAnswerModel;
import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by pristyanchandra on 1/25/17.
 */

public class BrandChallengeAdapter extends RecyclerView.Adapter {

    private ArrayList<BrandChallengeAnswerModel> brandChallengeAnswerModels;
    BrandListener brandListener;
    public int mSelectedItem = -1;
    boolean once = false;

    public BrandChallengeAdapter(Context context) {
        brandChallengeAnswerModels = new ArrayList<>();
    }

    public void update(ArrayList<BrandChallengeAnswerModel> brandChallengeAnswerModels) {
        this.brandChallengeAnswerModels = brandChallengeAnswerModels;
        once = true;
        notifyDataSetChanged();
    }

    public void reset(){

    }

    public void setBrandListener(BrandListener brandListener){
        this.brandListener = brandListener;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_radio, parent, false);
        return new Holder(v);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ((Holder) holder).setValue(brandChallengeAnswerModels.get(position));
        if (once){
            ((Holder) holder).rdItem.setChecked(false);
            return;
        }
        ((Holder) holder).rdItem.setChecked(position == mSelectedItem);
    }

    @Override
    public int getItemCount() {
        return brandChallengeAnswerModels.size();
    }

    public class Holder extends RecyclerView.ViewHolder {
        @Bind(R.id.item_radio) RadioButton rdItem;

        Holder(View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
        }

        private void setValue(final BrandChallengeAnswerModel brandChallengeAnswerModel) {
            rdItem.setChecked(true);
            rdItem.setText(brandChallengeAnswerModel.getBcAnswerAlphabet() +". "+ brandChallengeAnswerModel.getBcAnswer());
            View.OnClickListener clickListener = new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    once = false;
                    mSelectedItem = getAdapterPosition();
                    notifyItemRangeChanged(0, getItemCount());
                    brandListener.onClickRadio(brandChallengeAnswerModel);
                }
            };

            itemView.setOnClickListener(clickListener);
            rdItem.setOnClickListener(clickListener);
        }
    }

    public interface BrandListener{
        void onClickRadio(BrandChallengeAnswerModel brandChallengeAnswerModel);
    }

}
