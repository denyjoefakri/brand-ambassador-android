package brandambassador.fifgroup.com.fifgroupbrandambassador.activity;

import android.content.BroadcastReceiver;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.bilinedev.coreframework.connection.ServiceResponse;
import com.bilinedev.coreframework.ui.dialog.MessageDialog;
import com.bilinedev.coreframework.util.DateUtil;
import com.bilinedev.coreframework.util.DialogUtil;
import com.bilinedev.coreframework.util.ImageUtil;
import com.bilinedev.coreframework.util.PermissionUtil;
import com.bumptech.glide.Glide;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.HashMap;

import brandambassador.fifgroup.com.fifgroupbrandambassador.BaseActivity;
import brandambassador.fifgroup.com.fifgroupbrandambassador.R;
import brandambassador.fifgroup.com.fifgroupbrandambassador.connection.Filter;
import brandambassador.fifgroup.com.fifgroupbrandambassador.model.ProfileModel;
import brandambassador.fifgroup.com.fifgroupbrandambassador.util.Konstan;
import brandambassador.fifgroup.com.fifgroupbrandambassador.util.UserPreference;
import brandambassador.fifgroup.com.fifgroupbrandambassador.util.Util;
import butterknife.Bind;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by air-water on 2/5/17.
 */

public class ProfileActivity extends BaseActivity {

    ImageUtil imageUtil;
    File file;

    @Bind(R.id.profileImg) CircleImageView profileImg;
    @Bind(R.id.btnChangeProfile) TextView btnChangeProfile;
    @Bind(R.id.edFullname) EditText edFullname;
    @Bind(R.id.edEmail) EditText edEmail;
    @Bind(R.id.edNpk) EditText edNpk;
    @Bind(R.id.edCabang) EditText edCabang;
    @Bind(R.id.edNohp) EditText edNohp;

    UserPreference userPreference;
    BroadcastReceiver receiver = getReceiver();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        userPreference = UserPreference.getInstance(this);

        setNavigationProfile(this, getString(R.string.t_profile), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                userPreference.setUserId(null);
                userPreference.setUserEmail(null);
                userPreference.setUserFullName(null);
                userPreference.setUserNPK(null);
                userPreference.setDownloading(false);
                finish();
                DashboardActivity.getIntance().finish();
                startActivity(new Intent(ProfileActivity.this, LoginActivity.class));
            }
        });

        imageUtil = new ImageUtil(this);
        initComponents();

        if (getIntent().getStringExtra("fullname") != null || getIntent().getStringExtra("email") != null ||
                getIntent().getStringExtra("npk") != null || getIntent().getStringExtra("cabang") != null ||
                getIntent().getStringExtra("phone") != null || getIntent().getStringExtra("avatar") != null){

            edFullname.setText(getIntent().getStringExtra("fullname"));
            edEmail.setText(getIntent().getStringExtra("email"));
            edNpk.setText(getIntent().getStringExtra("npk"));
            edCabang.setText(getIntent().getStringExtra("cabang"));
            edNohp.setText(getIntent().getStringExtra("phone"));
            Glide.with(ProfileActivity.this).load(getIntent().getStringExtra("avatar")).error(R.drawable.ic_profile).into(profileImg);
        }
    }

    @Override
    protected int getResourceLayout() {
        return R.layout.activity_edit_profile;
    }

    @Override
    public void initComponents() {
        super.initComponents();

    }

    @OnClick(R.id.btnChangeProfile)
    public void changeProfile(){
        Util.hideSoftKeyboard(this);
        imageUtil.setFolderName("Ambassador_App");
        imageUtil.setFileName("APP_" + DateUtil.getCurrentDateTimeForPhoto());
        if (PermissionUtil.isCameraGranted(this, this, getSupportFragmentManager(), 0)) {
            imageUtil.openImageChooser(this, btnChangeProfile);
        }
    }

    @OnClick(R.id.btnSave)
    public void save(){
        Util.hideSoftKeyboard(this);

        if (file != null){
            Log.e("ada", "file");
            updateProfile(getHasmap(), getHasmapFile());
        } else {
            Log.e("tidak ada", "file");
            updateProfile(getHasmap(), null);
        }
        showProgressMessage(this, getString(R.string.p_please_wait), false);
    }

    private HashMap<String, String> getHasmap(){
        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put("id", userPreference.getUserId());
        hashMap.put("fullname", edFullname.getText().toString());
        hashMap.put("email", edEmail.getText().toString());
        hashMap.put("npk", edNpk.getText().toString());
        hashMap.put("cabang", edCabang.getText().toString());
        hashMap.put("phone", edNohp.getText().toString());
        //hashMap.put("password", "");

        return hashMap;
    }

    private HashMap<String, File> getHasmapFile(){
        HashMap<String, File> fileHashMap = new HashMap<>();
        fileHashMap.put("avatar", file);
        return fileHashMap;
    }


    @Override
    protected void onResume() {
        super.onResume();
        registerReceiver(receiver, new IntentFilter(Filter.GET_PROFILE));
    }

    @Override
    protected void onStop() {
        super.onStop();
        unregisterReceiver(receiver);
    }

    @Override
    public void successResponse(ServiceResponse response, String filter) {
        super.successResponse(response, filter);
        dismissProgressMessage();
        try {
            JSONObject object = new JSONObject(response.getContent());
            JSONObject dataObject = object.getJSONObject("data");
            DialogUtil.messageDialog(getSupportFragmentManager(), response.getMessage()).setOnMessageClosed(new MessageDialog.OnMessageClosed() {
                @Override
                public void onClosed() {
                    finish();
                }
            });

        } catch (JSONException e){

        }
    }

    @Override
    public void badResponse(ServiceResponse response, String filter) {
        super.badResponse(response, filter);
        dismissProgressMessage();
        DialogUtil.messageDialog(getSupportFragmentManager(), response.getMessage());
    }

    @Override
    public void onTimeout(String filter) {
        super.onTimeout(filter);
        dismissProgressMessage();
        DialogUtil.messageDialog(getSupportFragmentManager(), getString(R.string.msg_rto));
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (imageUtil.onHandleImage(requestCode, resultCode, data) != null){
            file = ImageUtil.compressImage(this, imageUtil.onHandleImage(requestCode, resultCode, data));
            Glide.with(this).load(file).error(R.drawable.ic_profile).into(profileImg);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED
                && grantResults[1] == PackageManager.PERMISSION_GRANTED
                && grantResults[2] == PackageManager.PERMISSION_GRANTED) {
            imageUtil.openImageChooser(this, btnChangeProfile);
        } else {
            Util.initToast(this, getString(R.string.p_permissions_denied), Konstan.Toast_LENGTH_SHORT);
        }
    }

}
