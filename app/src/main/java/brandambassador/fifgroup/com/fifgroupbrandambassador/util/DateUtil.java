package brandambassador.fifgroup.com.fifgroupbrandambassador.util;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by dev_deny on 3/7/17.
 */

public class DateUtil {
    public String date(){
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date date = new Date();
        if (hour() >= 14){
            Calendar c = Calendar.getInstance();
            c.setTime(date);
            c.add(Calendar.DATE, 1);
            date = c.getTime();
            return dateFormat.format(date);
        } else {
            return dateFormat.format(date);
        }
    }

    public int hour(){
        Calendar now= Calendar.getInstance();
        int hour = now.get(Calendar.HOUR_OF_DAY);
        int minute=now.get(Calendar.MINUTE);
        return hour;
    }
}
