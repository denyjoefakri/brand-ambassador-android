package brandambassador.fifgroup.com.fifgroupbrandambassador.connection;


import android.content.Context;

import com.bilinedev.coreframework.connection.ClientServiceDialog;
import com.bilinedev.coreframework.ui.BaseActivity;
import com.tsengvn.typekit.TypekitContextWrapper;

import java.io.File;
import java.util.HashMap;

import brandambassador.fifgroup.com.fifgroupbrandambassador.BuildConfig;

/**
 * Created by telolet on 10/18/16.
 */

public class ServiceDialog extends ClientServiceDialog {


    public void getListRedeem() {
        sendGetRequest(BuildConfig.SITE + Path.LIST_REDEEM, null, Filter.GET_LIST_REDEEM, BuildConfig.KEY);
    }

}
