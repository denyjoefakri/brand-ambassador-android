package brandambassador.fifgroup.com.fifgroupbrandambassador.model;

import java.io.Serializable;

/**
 * Created by air-water on 2/13/17.
 */

public class HistoryModel implements Serializable {

    private String typeBrands;
    private String typeId;
    private String date;
    private String title;
    private String status;

    public String getTypeBrands() {
        return typeBrands;
    }

    public void setTypeBrands(String typeBrands) {
        this.typeBrands = typeBrands;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getTypeId() {
        return typeId;
    }

    public void setTypeId(String typeId) {
        this.typeId = typeId;
    }
}
