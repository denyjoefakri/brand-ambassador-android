package brandambassador.fifgroup.com.fifgroupbrandambassador;

import android.app.Application;
import android.content.Context;
import android.support.multidex.MultiDex;

import com.tsengvn.typekit.Typekit;

/**
 * Created by pristyanchandra on 2/6/17.
 */

public class MyApp extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        Typekit.getInstance()
                .addNormal(Typekit.createFromAsset(this, getString(R.string.font_regular)))
                .addBold(Typekit.createFromAsset(this, getString(R.string.font_bold)))
                .addItalic(Typekit.createFromAsset(this, getString(R.string.font_italic)))
                .addBoldItalic(Typekit.createFromAsset(this, getString(R.string.font_bolditalic)));
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }
}
