package brandambassador.fifgroup.com.fifgroupbrandambassador.activity;

import android.content.BroadcastReceiver;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.bilinedev.coreframework.connection.ServiceResponse;
import com.bilinedev.coreframework.ui.dialog.MessageDialog;
import com.bilinedev.coreframework.util.DialogUtil;
import com.bilinedev.coreframework.util.ValidationUtil;
import com.google.firebase.iid.FirebaseInstanceId;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

import brandambassador.fifgroup.com.fifgroupbrandambassador.BaseActivity;
import brandambassador.fifgroup.com.fifgroupbrandambassador.R;
import brandambassador.fifgroup.com.fifgroupbrandambassador.connection.Filter;
import brandambassador.fifgroup.com.fifgroupbrandambassador.util.TexwatcherAdapter;
import brandambassador.fifgroup.com.fifgroupbrandambassador.util.UserPreference;
import brandambassador.fifgroup.com.fifgroupbrandambassador.util.Util;
import butterknife.Bind;
import butterknife.OnClick;

/**
 * Created by air-water on 2/5/17.
 */

public class RegisterActivity extends BaseActivity {

    @Bind(R.id.edFullname) EditText edFullname;
    @Bind(R.id.edEmail) EditText edEmail;
    @Bind(R.id.edPassword) EditText edPassword;
    @Bind(R.id.edPasswordConfirm) EditText edPasswordConfirm;
    @Bind(R.id.edNpk) EditText edNpk;
    @Bind(R.id.edCabang) EditText edCabang;
    @Bind(R.id.edNohp) EditText edNohp;
    @Bind(R.id.btnSignin) Button btnSignin;

    BroadcastReceiver receiver = getReceiver();

    UserPreference userPreference;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initComponents();
    }

    @Override
    protected int getResourceLayout() {
        return R.layout.activity_register;
    }

    @Override
    public void initComponents() {
        super.initComponents();
        edPasswordConfirm.addTextChangedListener(texwatcherAdapter);

        userPreference = UserPreference.getInstance(this);

        btnSignin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.e("fcm_id", FirebaseInstanceId.getInstance().getToken());
                if (isComplete()){
                    Util.hideSoftKeyboard(RegisterActivity.this);
                    HashMap<String, String> map = new HashMap<>();
                    map.put("fullname", edFullname.getText().toString());
                    map.put("email", edEmail.getText().toString());
                    map.put("password", edPassword.getText().toString());
                    map.put("npk", edNpk.getText().toString());
                    map.put("cabang", edCabang.getText().toString());
                    map.put("phone", edNohp.getText().toString());
                    map.put("gcm_id", FirebaseInstanceId.getInstance().getToken());

                    postRegister(map);
                    showProgressMessage(RegisterActivity.this, getString(R.string.p_please_wait), false);
                }
            }
        });
    }


    @Override
    protected void onResume() {
        super.onResume();
        registerReceiver(receiver, new IntentFilter(Filter.GET_REGISTER));
    }

    @Override
    protected void onStop() {
        super.onStop();
        unregisterReceiver(receiver);
    }

    @Override
    public void successResponse(ServiceResponse response, String filter) {
        super.successResponse(response, filter);
        dismissProgressMessage();
        try {
            JSONObject object = new JSONObject(response.getContent());
            JSONObject objectData = object.getJSONObject("data");
            userPreference.setUserId(objectData.getString("id"));
            userPreference.setUserEmail(objectData.getString("email"));
            userPreference.setUserFullName(objectData.getString("fullname"));
            userPreference.setUserNPK(objectData.getString("npk"));
            DialogUtil.messageDialog(getSupportFragmentManager(), response.getMessage()).setOnMessageClosed(new MessageDialog.OnMessageClosed() {
                @Override
                public void onClosed() {
                    startActivity(new Intent(RegisterActivity.this, DashboardActivity.class));
                    LoginActivity.getInstance().finish();
                    finish();
                }
            });
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void badResponse(ServiceResponse response, String filter) {
        super.badResponse(response, filter);
        dismissProgressMessage();
        DialogUtil.messageDialog(getSupportFragmentManager(), response.getMessage());
    }

    @Override
    public void onTimeout(String filter) {
        super.onTimeout(filter);
        dismissProgressMessage();
        DialogUtil.messageDialog(getSupportFragmentManager(), getString(R.string.msg_rto));
    }

    TexwatcherAdapter texwatcherAdapter = new TexwatcherAdapter(){
        @Override
        public void afterTextChanged(Editable s) {
            if (edPassword.getText().toString().isEmpty()){
                edPassword.setError(getString(R.string.er_password_empty));
                edPasswordConfirm.setError(getString(R.string.er_password_not_match));
            } else {
                if (!edPasswordConfirm.getText().toString().equals(edPassword.getText().toString())){
                    edPassword.setError(getString(R.string.er_password_not_match));
                    edPasswordConfirm.setError(getString(R.string.er_password_not_match));
                } else {
                    edPassword.setError(null);
                    edPasswordConfirm.setError(null);
                }
            }
        }
    };

    private boolean isComplete() {
        edFullname.setError(null);
        edEmail.setError(null);
        edNpk.setError(null);
        edCabang.setError(null);
        edNohp.setError(null);
        edPassword.setError(null);
        edPasswordConfirm.setError(null);

        if(!ValidationUtil.isValidEmail(edEmail.getText().toString())){
            edEmail.setError(getString(R.string.invalid_email_message));
            return false;
        }

        if(edFullname.getText().toString().equals("")){
            edFullname.setError(getString(R.string.fullname_empty_message));
            return false;
        }

        if(edNpk.getText().toString().equals("")){
            edNpk.setError(getString(R.string.npk_empty_message));
            return false;
        }

        if(edCabang.getText().toString().equals("")){
            edCabang.setError(getString(R.string.cabang_empty_message));
            return false;
        }

        if(edNohp.getText().toString().equals("")){
            edNohp.setError(getString(R.string.nohp_empty_message));
            return false;
        }

        return true;
    }

}
