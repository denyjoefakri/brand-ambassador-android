package brandambassador.fifgroup.com.fifgroupbrandambassador.model;

import java.io.Serializable;

/**
 * Created by air-water on 2/26/17.
 */

public class ProfileModel implements Serializable {

    private String id;
    private String fullname;
    private String email;
    private String npk;
    private String cabang;
    private String phone;
    private String avatar;
    private String point;
    private String point_total;
    private String active;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getNpk() {
        return npk;
    }

    public void setNpk(String npk) {
        this.npk = npk;
    }

    public String getCabang() {
        return cabang;
    }

    public void setCabang(String cabang) {
        this.cabang = cabang;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getPoint() {
        return point;
    }

    public void setPoint(String point) {
        this.point = point;
    }

    public String getPoint_total() {
        return point_total;
    }

    public void setPoint_total(String point_total) {
        this.point_total = point_total;
    }

    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }
}
