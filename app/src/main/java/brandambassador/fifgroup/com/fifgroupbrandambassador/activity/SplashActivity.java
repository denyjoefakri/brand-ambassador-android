package brandambassador.fifgroup.com.fifgroupbrandambassador.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;

import brandambassador.fifgroup.com.fifgroupbrandambassador.BaseActivity;
import brandambassador.fifgroup.com.fifgroupbrandambassador.R;
import brandambassador.fifgroup.com.fifgroupbrandambassador.util.UserPreference;

/**
 * Created by air-water on 2/5/17.
 */

public class SplashActivity extends BaseActivity {

    UserPreference userPreference;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        userPreference = UserPreference.getInstance(this);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (userPreference.getUserId() == null){
                    Intent i = new Intent(SplashActivity.this, LoginActivity.class);
                    startActivity(i);
                    finish();
                } else {
                    Intent i = new Intent(SplashActivity.this, DashboardActivity.class);
                    startActivity(i);
                    finish();
                }
            }
        }, 1000);
    }

    @Override
    protected int getResourceLayout() {
        return R.layout.activity_splash;
    }




}
