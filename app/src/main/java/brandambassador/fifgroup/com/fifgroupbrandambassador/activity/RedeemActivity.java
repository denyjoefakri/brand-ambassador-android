package brandambassador.fifgroup.com.fifgroupbrandambassador.activity;

import android.content.BroadcastReceiver;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;

import com.bilinedev.coreframework.connection.ServiceResponse;
import com.bilinedev.coreframework.ui.dialog.MessageDialog;
import com.bilinedev.coreframework.ui.widget.spinner.SingleSpinnerAdapter;
import com.bilinedev.coreframework.util.DialogUtil;
import com.bumptech.glide.Glide;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import brandambassador.fifgroup.com.fifgroupbrandambassador.BaseActivity;
import brandambassador.fifgroup.com.fifgroupbrandambassador.R;
import brandambassador.fifgroup.com.fifgroupbrandambassador.connection.Filter;
import brandambassador.fifgroup.com.fifgroupbrandambassador.connection.Path;
import brandambassador.fifgroup.com.fifgroupbrandambassador.dialog.DialogForgotPassword;
import brandambassador.fifgroup.com.fifgroupbrandambassador.dialog.DialogRedeem;
import brandambassador.fifgroup.com.fifgroupbrandambassador.util.DialogUtilCustom;
import brandambassador.fifgroup.com.fifgroupbrandambassador.util.UserPreference;
import brandambassador.fifgroup.com.fifgroupbrandambassador.util.Util;
import butterknife.Bind;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by air-water on 2/5/17.
 */

public class RedeemActivity extends BaseActivity {

    BroadcastReceiver receiver = getReceiver();
    UserPreference userPreference;

    @Bind(R.id.scrollView) ScrollView scrollView;
    @Bind(R.id.txtPoint) TextView txtPoint;
    @Bind(R.id.imgGift) ImageView imgGift;
    @Bind(R.id.spnListRedeem) Spinner spnListRedeem;
    @Bind(R.id.pbListRedeem) ProgressBar pbListRedeem;
    @Bind(R.id.edPoint) EditText edPoint;

    int pos;
    ArrayList<String> point;
    ArrayList<String> code;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setNavigationBackText(this, getString(R.string.t_redeem));
        userPreference = UserPreference.getInstance(this);
        point = new ArrayList<>();
        code = new ArrayList<>();

        if (getIntent().getStringExtra("reward_points") != null){
            txtPoint.setText(getIntent().getStringExtra("reward_points")+ " " + getString(R.string.msg_points));
        }

        Glide.with(this).load(Path.IMAGE_REDEEM).error(R.drawable.image_landscape).into(imgGift);
        getListRedeem();

    }

    @Override
    protected int getResourceLayout() {
        return R.layout.activity_redeem;
    }

    @OnClick(R.id.btnRedeem)
    public void redeem(){
        HashMap<String, String> map = new HashMap<>();
        map.put("redeem", edPoint.getText().toString());
        map.put("employee_id", userPreference.getUserId());
        map.put("code", code.get(pos));
        postRedeem(map);
        showProgressMessage(RedeemActivity.this, getString(R.string.p_please_wait), false);
        /*DialogUtilCustom.dialogRedeem(getSupportFragmentManager(), "").setOnRedeem(new DialogRedeem.OnRedeem() {
            @Override
            public void onRedeem(String point) {
                Log.e("point", point);

            }
        });*/
    }

    @Override
    protected void onResume() {
        super.onResume();
        registerReceiver(receiver, new IntentFilter(Filter.GET_REDEEM));
        registerReceiver(receiver, new IntentFilter(Filter.GET_LIST_REDEEM));

        spinner();
    }

    @Override
    protected void onStop() {
        super.onStop();
        unregisterReceiver(receiver);
    }

    public void spinner(){
        spnListRedeem.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                pos = position;
                edPoint.setText(point.get(position));
                Util.hideSoftKeyboard(RedeemActivity.this);

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }

    @Override
    public void successResponse(ServiceResponse response, String filter) {
        super.successResponse(response, filter);
        dismissProgressMessage();
        switch (filter){
            case Filter.GET_LIST_REDEEM:
                pbListRedeem.setVisibility(View.GONE);
                try {
                    JSONObject jsonObject = new JSONObject(response.getContent());
                    JSONArray jsonArray = jsonObject.getJSONArray("data");
                    ArrayList<String> spin = new ArrayList<>();
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject object = jsonArray.getJSONObject(i);
                        spin.add(object.getString("name"));
                        point.add(object.getString("point"));
                        code.add(object.getString("code"));
                    }
                    spnListRedeem.setAdapter(new SingleSpinnerAdapter(this, getLayoutInflater(), spin));
                }catch (JSONException e){
                    e.printStackTrace();
                }
                break;
            case Filter.GET_REDEEM:
                DialogUtil.messageDialog(getSupportFragmentManager(), response.getMessage()).setOnMessageClosed(new MessageDialog.OnMessageClosed() {
                    @Override
                    public void onClosed() {
                        finish();
                    }
                });
                break;
        }

    }

    @Override
    public void badResponse(ServiceResponse response, String filter) {
        super.badResponse(response, filter);
        dismissProgressMessage();
        pbListRedeem.setVisibility(View.GONE);
        DialogUtil.messageDialog(getSupportFragmentManager(), response.getMessage());
    }

    @Override
    public void onTimeout(String filter) {
        super.onTimeout(filter);
        dismissProgressMessage();
        pbListRedeem.setVisibility(View.GONE);
        DialogUtil.messageDialog(getSupportFragmentManager(), getString(R.string.msg_rto));
    }
}
