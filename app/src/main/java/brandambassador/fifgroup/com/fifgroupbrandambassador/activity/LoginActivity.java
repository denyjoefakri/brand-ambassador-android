package brandambassador.fifgroup.com.fifgroupbrandambassador.activity;

import android.content.BroadcastReceiver;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.util.Log;
import android.widget.EditText;

import com.bilinedev.coreframework.connection.ServiceResponse;
import com.bilinedev.coreframework.ui.dialog.MessageDialog;
import com.bilinedev.coreframework.util.DialogUtil;
import com.bilinedev.coreframework.util.ValidationUtil;
import com.google.firebase.iid.FirebaseInstanceId;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

import brandambassador.fifgroup.com.fifgroupbrandambassador.BaseActivity;
import brandambassador.fifgroup.com.fifgroupbrandambassador.R;
import brandambassador.fifgroup.com.fifgroupbrandambassador.connection.Filter;
import brandambassador.fifgroup.com.fifgroupbrandambassador.dialog.DialogForgotPassword;
import brandambassador.fifgroup.com.fifgroupbrandambassador.dialog.MessageDialogAnswer;
import brandambassador.fifgroup.com.fifgroupbrandambassador.util.DialogUtilCustom;
import brandambassador.fifgroup.com.fifgroupbrandambassador.util.Konstan;
import brandambassador.fifgroup.com.fifgroupbrandambassador.util.UserPreference;
import brandambassador.fifgroup.com.fifgroupbrandambassador.util.Util;
import butterknife.Bind;
import butterknife.OnClick;

/**
 * Created by air-water on 2/5/17.
 */

public class LoginActivity extends BaseActivity {

    UserPreference userPreference;
    BroadcastReceiver receiver = getReceiver();

    @Bind(R.id.login_npk) EditText loginNpk;
    @Bind(R.id.login_password) EditText loginPassword;
    public static LoginActivity loginActivity;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        loginActivity = this;
        initComponents();
    }

    @Override
    protected int getResourceLayout() {
        return R.layout.activity_login;
    }

    @Override
    public void initComponents() {
        userPreference = UserPreference.getInstance(this);

    }

    @OnClick (R.id.btnRegister)
    public void register(){
        Util.hideSoftKeyboard(this);
        startActivity(new Intent(LoginActivity.this, RegisterActivity.class));
    }

    @OnClick(R.id.btnSignin)
    public void signin(){
        Log.e("fcm_id", FirebaseInstanceId.getInstance().getToken());
        if (isComplete()){
            Util.hideSoftKeyboard(this);
            HashMap<String, String> map = new HashMap<>();
            map.put("npk", loginNpk.getText().toString());
            map.put("password", loginPassword.getText().toString());
            map.put("gcm_id", FirebaseInstanceId.getInstance().getToken());
            postLogin(map);
            showProgressMessage(LoginActivity.this, getString(R.string.p_please_wait), false);
        }
    }

    @OnClick(R.id.btnForgotPassword)
    public void forgot(){
        DialogUtilCustom.forgotPasswordDialog(getSupportFragmentManager(), "").setOnForgotPassword(new DialogForgotPassword.OnForgotPassword() {
            @Override
            public void onForgot(String email) {
                HashMap<String, String> map = new HashMap<>();
                map.put("email", email);
                postForgotPassword(map);
                showProgressMessage(LoginActivity.this, getString(R.string.p_please_wait), false);
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        registerReceiver(receiver, new IntentFilter(Filter.GET_LOGIN));
        registerReceiver(receiver, new IntentFilter(Filter.GET_FORGOT_PASSWORD));
    }

    @Override
    protected void onStop() {
        super.onStop();
        unregisterReceiver(receiver);
    }

    @Override
    public void successResponse(ServiceResponse response, String filter) {
        super.successResponse(response, filter);
        dismissProgressMessage();
        switch (filter){
            case Filter.GET_LOGIN:
                try {
                    JSONObject object = new JSONObject(response.getContent());
                    JSONObject objectData = object.getJSONObject("data");
                    userPreference.setUserId(objectData.getString("id"));
                    userPreference.setUserEmail(objectData.getString("email"));
                    userPreference.setUserFullName(objectData.getString("fullname"));
                    userPreference.setUserNPK(objectData.getString("npk"));
                    Intent intent = new Intent(LoginActivity.this, DashboardActivity.class);
                    intent.putExtra(Konstan.MESSAGE, response.getMessage());
                    startActivity(intent);
                    finish();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                break;
            case Filter.GET_FORGOT_PASSWORD:
                DialogUtil.messageDialog(getSupportFragmentManager(), response.getMessage());
                break;
        }


    }

    @Override
    public void badResponse(ServiceResponse response, String filter) {
        super.badResponse(response, filter);
        dismissProgressMessage();
        DialogUtil.messageDialog(getSupportFragmentManager(), response.getMessage());
    }

    @Override
    public void onTimeout(String filter) {
        super.onTimeout(filter);
        dismissProgressMessage();
        DialogUtil.messageDialog(getSupportFragmentManager(), getString(R.string.msg_rto));
    }

    private boolean isComplete() {
        loginNpk.setError(null);
        loginPassword.setError(null);

        if(loginNpk.getText().toString().equals("")){
            loginNpk.setError(getString(R.string.invalid_email_message));
            return false;
        }

        if(loginPassword.getText().toString().equals("")){
            loginPassword.setError(getString(R.string.er_password_empty));
            return false;
        }

        return true;
    }

    public static LoginActivity getInstance(){
        return loginActivity;
    }

}
