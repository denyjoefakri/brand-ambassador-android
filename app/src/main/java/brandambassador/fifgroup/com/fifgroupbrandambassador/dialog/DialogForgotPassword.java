package brandambassador.fifgroup.com.fifgroupbrandambassador.dialog;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.EditText;
import android.widget.TextView;

import brandambassador.fifgroup.com.fifgroupbrandambassador.R;


/**
 * Created by pristyanchandra on 10/14/16.
 */
public class DialogForgotPassword extends DialogFragment {

    private OnForgotPassword forgotPassword;

    public static DialogForgotPassword newIntance(String message) {
        DialogForgotPassword messageDialog = new DialogForgotPassword();
        Bundle bundle = new Bundle();
        bundle.putCharSequence("message", message);
        messageDialog.setArguments(bundle);
        return messageDialog;
    }

    public void setOnForgotPassword(OnForgotPassword forgotPassword) {
        this.forgotPassword = forgotPassword;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.dialog_forgot_password, container, false);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NO_TITLE, R.style.CustomDialog);
    }

    @Override
    public void onViewCreated(View v, @Nullable Bundle savedInstanceState) {
        getDialog().requestWindowFeature(Window.FEATURE_NO_TITLE);
        setStyle(STYLE_NO_FRAME, 0);

        final EditText edEmail = (EditText) v.findViewById(R.id.ed_nama_email);
        TextView btnForgot = (TextView) v.findViewById(R.id.dialog_forgot_password);
        btnForgot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (forgotPassword != null) {
                    forgotPassword.onForgot(edEmail.getText().toString());
                }
                dismiss();
            }
        });

    }

    public interface OnForgotPassword {
        public void onForgot(String email);
    }
}