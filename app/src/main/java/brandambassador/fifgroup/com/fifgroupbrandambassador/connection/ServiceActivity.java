package brandambassador.fifgroup.com.fifgroupbrandambassador.connection;


import android.content.Context;

import com.bilinedev.coreframework.ui.BaseActivity;
import com.tsengvn.typekit.TypekitContextWrapper;

import java.io.File;
import java.util.HashMap;

import brandambassador.fifgroup.com.fifgroupbrandambassador.BuildConfig;

/**
 * Created by telolet on 10/18/16.
 */

public class ServiceActivity extends BaseActivity {

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(TypekitContextWrapper.wrap(newBase));
    }

    /**Login*/
    public void postRegister(HashMap<String,String> map) {
        sendPostRequest(BuildConfig.SITE + Path.REGISTER, map, Filter.GET_REGISTER, BuildConfig.KEY);
    }

    public void postForgotPassword(HashMap<String,String> map) {
        sendPostRequest(BuildConfig.SITE + Path.FORGOT_PASSWORD, map, Filter.GET_FORGOT_PASSWORD, BuildConfig.KEY);
    }

    public void getProfile(HashMap<String,String> map) {
        sendGetRequest(BuildConfig.SITE + Path.PROFILE, map, Filter.GET_PROFILE, BuildConfig.KEY);
    }

    public void updateProfile(HashMap<String,String> map, HashMap<String, File> file) {
        sendPostWithFile(BuildConfig.SITE + Path.PROFILE, map, file, Filter.GET_PROFILE, BuildConfig.KEY);
    }

    public void postRedeem(HashMap<String,String> map) {
        sendPostRequest(BuildConfig.SITE + Path.REDEEM, map, Filter.GET_REDEEM, BuildConfig.KEY);
    }

    public void postLogin(HashMap<String,String> map) {
        sendPostRequest(BuildConfig.SITE + Path.LOGIN, map, Filter.GET_LOGIN, BuildConfig.KEY);
    }

    public void postBrandDetective(HashMap<String,String> map, HashMap<String, File> mapFile) {
        sendPostWithFile(BuildConfig.SITE + Path.POST_BRAND_DETECTIVE, map, mapFile, Filter.POST_BRAND_DETECTIVE, BuildConfig.KEY);
    }

    public void getBrandChallenge(HashMap<String,String> map) {
        sendGetRequest(BuildConfig.SITE + Path.GET_BRAND_CHALLENGE, map, Filter.GET_BRAND_CHALLENGE, BuildConfig.KEY);
    }

    public void postBrandChallenge(HashMap<String,String> map) {
        sendPostRequest(BuildConfig.SITE + Path.POST_BRAND_CHALLENGE, map, Filter.POST_BRAND_CHALLENGE, BuildConfig.KEY);
    }

    public void postBrandPolice(HashMap<String,String> map, HashMap<String, File> mapFile) {
        sendPostWithFile(BuildConfig.SITE + Path.POST_BRAND_POLICE, map, mapFile, Filter.POST_BRAND_POLICE, BuildConfig.KEY);
    }

    public void getHistory(HashMap<String,String> map) {
        sendGetRequest(BuildConfig.SITE + Path.HISTORY, map, Filter.GET_HISTORY, BuildConfig.KEY);
    }

    public void postReadNotif(HashMap<String,String> map) {
        sendPostRequest(BuildConfig.SITE + Path.NOTIFICATION, map, Filter.POST_NOTIFICATION, BuildConfig.KEY);
    }

    public void getNotification(HashMap<String,String> map) {
        sendGetRequest(BuildConfig.SITE + Path.NOTIFICATION, map, Filter.GET_NOTIFICATION, BuildConfig.KEY);
    }

    public void getListRedeem() {
        sendGetRequest(BuildConfig.SITE + Path.LIST_REDEEM, null, Filter.GET_LIST_REDEEM, BuildConfig.KEY);
    }

}
