package brandambassador.fifgroup.com.fifgroupbrandambassador.util;

import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.FileProvider;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.Toast;

import com.bilinedev.coreframework.util.imagecompressor.Compressor;
import com.bumptech.glide.Glide;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import brandambassador.fifgroup.com.fifgroupbrandambassador.R;

import static android.content.Context.INPUT_METHOD_SERVICE;

/**
 * Created by telolet on 18/10/16.
 */

public class Util {

    /** TOAST */
    public static void initToast(Context c, String message, int type){
        if (type == Konstan.Toast_LENGTH_SHORT){
            Toast.makeText(c, message, Toast.LENGTH_SHORT).show();
        }else {
            Toast.makeText(c, message, Toast.LENGTH_LONG).show();
        }
    }
    /** SNACKBAR */
    public static void initSnackbar(Context context, View view, int resPesan, View.OnClickListener listenersnackbar, String btnMsg) {
        Snackbar.make(view, resPesan, Snackbar.LENGTH_LONG).setAction(btnMsg, listenersnackbar)
                .setActionTextColor(context.getResources().getColor(R.color.white)).show();
    }

    /** HideKeyboard */
    public static void hidekeyboard(Context context, View view) {
        InputMethodManager manager = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        manager.hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.RESULT_UNCHANGED_SHOWN);
    }

    /** TextInput Layout Error */
    public static void setError(Context context, EditText edits, TextInputLayout textInputLayout, int resId) {
        edits.requestFocus();
        textInputLayout.setError(context.getResources().getString(resId));
        Log.w("SET ERROR", "SET ERROR");
    }

    public static void hideSoftKeyboard(Activity activity) {
        if (activity.getCurrentFocus() != null) {
            InputMethodManager inputMethodManager = (InputMethodManager) activity.getSystemService(INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), 0);
        }
    }

    public static boolean checkConnection(Context context) {
        boolean conectado;
        ConnectivityManager conectivtyManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        conectado = conectivtyManager.getActiveNetworkInfo() != null
                && conectivtyManager.getActiveNetworkInfo().isAvailable()
                && conectivtyManager.getActiveNetworkInfo().isConnected();
        return conectado;
    }

    /*public static ImageView likeButton(Context ctx, View v, final ImageView imageView){
        final Animation animScale = AnimationUtils.loadAnimation(ctx, R.anim.anim_scale);

        v.startAnimation(animScale);

        animScale.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                //imageView.setImageResource(R.drawable.ic_favorite_black_24dp);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });

        return imageView;
    }

    public static ImageView dislikeButton(Context ctx, View v, final ImageView imageView){
        final Animation animScale = AnimationUtils.loadAnimation(ctx, R.anim.anim_scale);

        v.startAnimation(animScale);

        animScale.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                //imageView.setImageResource(R.drawable.ic_favorite_border_black_24dp);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });

        return imageView;
    }

    public static FragmentTransaction commit(Context context, String TAG, FrameLayout fragmentContainer, FragmentManager manager, Fragment fragment){
        FragmentManager fragmentManager = manager;
        FragmentTransaction fragmentTransaction = manager.beginTransaction();
        fragmentTransaction.replace(R.id.frag_containers, fragment, TAG);
        //fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
        if (fragmentContainer != null) {
            Animation fadeIn = AnimationUtils.loadAnimation(context, R.anim.fade_in);
            fragmentContainer.startAnimation(fadeIn);
        }

        return fragmentTransaction;
    }*/

    public static String getRealPathFromURI(Uri content, Context ctx){
        String result;
        Cursor cursor = ctx.getContentResolver().query(content,null,null,null,null);
        if(cursor==null){
            result = content.getPath();
        } else {
            cursor.moveToFirst();
            int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            result = cursor.getString(idx);
            cursor.close();
        }
        return result;
    }

    public static Uri getPhotoFileUri(Context context) {
        if (isExternalStorageAvailable()) {
            File destination = new File(Environment.getExternalStorageDirectory(), "Postwork");
            if (!destination.exists() && !destination.mkdirs()){
                Log.d("Main", "failed to create directory");
            }
            File compress = new File(destination.getPath() + File.separator + System.currentTimeMillis() + ".jpg");
            if (Build.VERSION.SDK_INT >= 24) {
                return FileProvider.getUriForFile(context, context.getPackageName() + ".provider", compress);
            }else {
                return Uri.fromFile(compress);
            }
        }
        return null;
    }

    private static boolean isExternalStorageAvailable() {
        String state = Environment.getExternalStorageState();
        return state.equals(Environment.MEDIA_MOUNTED);
    }

    public static File compressImageString(Context context, String file){
        File compressedImage = new Compressor.Builder(context)
                .setMaxWidth(768)
                .setMaxHeight(1280)
                .setQuality(100)
                .setCompressFormat(Bitmap.CompressFormat.JPEG)
                /*.setDestinationDirectoryPath(Environment.getExternalStoragePublicDirectory(
                        Environment.DIRECTORY_PICTURES).getAbsolutePath())*/
                .build()
                .compressToFile(new File(file));
        return compressedImage;
    }

    public static File compressImageFile(Context context, File file){
        File compressedImage = new Compressor.Builder(context)
                .setMaxWidth(768)
                .setMaxHeight(1280)
                .setQuality(100)
                .setCompressFormat(Bitmap.CompressFormat.JPEG)
                /*.setDestinationDirectoryPath(Environment.getExternalStoragePublicDirectory(
                        Environment.DIRECTORY_PICTURES).getAbsolutePath())*/
                .build()
                .compressToFile(file);
        return compressedImage;
    }

    public static String mergeWord(ArrayList<String> word){
        String data = "";
        if (word.size() == 1){
            data = word.get(0);
        }else {
            for (int i = 0; i < word.size(); i++) {
                if (i == 0) {
                    data = word.get(i) + ",";
                } else if (i == (word.size() - 1)) {
                    data = data + word.get(i);
                } else {
                    data = data + word.get(i) + ",";
                }
            }
        }
        return data;
    }

    public static File getDir() {
        File mediaStorageDir = new File(Environment.getExternalStorageDirectory().getPath(), "Fit2Go");
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                return null;
            }
        }

        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String timeStamp2 = new SimpleDateFormat("dd MMM yyyy HH:mm:ss").format(new Date());
        File mediaFile;
        mediaFile = new File(mediaStorageDir.getPath() + File.separator + "FIT2GO_" + timeStamp + ".jpg");
        /*date = timeStamp2;
        path = mediaFile.getAbsolutePath();
        fileName = "Postwork_" + timeStamp + ".jpg";*/
        return mediaFile;
    }

    public static void setImage(Context context, int drawable, ImageView imageView){
        Glide.with(context).load(drawable).error(R.drawable.ic_profile).into(imageView);
    }

    public static String getMonth(int month) {
        String res;
        switch (month) {
            case 0:
                res = "Jan";
                break;
            case 1:
                res = "Feb";
                break;
            case 2:
                res = "Mar";
                break;
            case 3:
                res = "Apr";
                break;
            case 4:
                res = "May";
                break;
            case 5:
                res = "Jun";
                break;
            case 6:
                res = "Jul";
                break;
            case 7:
                res = "Aug";
                break;
            case 8:
                res = "Sep";
                break;
            case 9:
                res = "Oct";
                break;
            case 10:
                res = "Nov";
                break;
            default:
                res = "Dec";
                break;
        }
        return res;
    }

}
