package brandambassador.fifgroup.com.fifgroupbrandambassador.model;

import java.io.Serializable;

/**
 * Created by air-water on 2/13/17.
 */

public class NotificationModel implements Serializable {

    private String date;
    private String title;

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

}
