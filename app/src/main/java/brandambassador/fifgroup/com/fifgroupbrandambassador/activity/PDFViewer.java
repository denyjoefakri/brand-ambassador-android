package brandambassador.fifgroup.com.fifgroupbrandambassador.activity;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.View;
import android.webkit.WebView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;


import org.apache.commons.io.IOUtils;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.concurrent.TimeUnit;

import brandambassador.fifgroup.com.fifgroupbrandambassador.BaseActivity;
import brandambassador.fifgroup.com.fifgroupbrandambassador.R;
import brandambassador.fifgroup.com.fifgroupbrandambassador.connection.Path;
import brandambassador.fifgroup.com.fifgroupbrandambassador.connection.remote.DownloadFile;
import brandambassador.fifgroup.com.fifgroupbrandambassador.connection.remote.DownloadFileUrlConnectionImpl;
import brandambassador.fifgroup.com.fifgroupbrandambassador.connection.remote.FileDownloader;
import brandambassador.fifgroup.com.fifgroupbrandambassador.util.Konstan;
import brandambassador.fifgroup.com.fifgroupbrandambassador.util.RingProgressBar;
import brandambassador.fifgroup.com.fifgroupbrandambassador.util.Util;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by air-water on 2/23/17.
 */

public class PDFViewer extends BaseActivity{

    //PDFView pdfView;
    RelativeLayout pbPDF;
    RingProgressBar progressBar;
    TextView progressText;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setNavigationBackText(this, getString(R.string.t_pdf));

        pbPDF = (RelativeLayout) findViewById(R.id.pbPDF);
        progressBar = (RingProgressBar) findViewById(R.id.progress_bar_loading);
        progressText = (TextView) findViewById(R.id.progress_text);
        //pdfView = (PDFView) findViewById(R.id.pdfView);


        pbPDF.setVisibility(View.GONE);


        new DownloadFile().execute(Path.PDF, "brand.pdf");

        //new PdfStream().execute(Path.PDF);

        /*if (progressBar.getMax() != total) {
            progressBar.setMax(total);
        }
        progressText.setText(String.valueOf(progress));
        progressBar.setProgress(progress);*/

    }



    @Override
    protected int getResourceLayout() {
        return R.layout.activity_pdf;
    }


    private class DownloadFile extends AsyncTask<String, Void, Void>{

        @Override
        protected Void doInBackground(String... strings) {
            String fileUrl = strings[0];   // -> http://maven.apache.org/maven-1.x/maven.pdf
            String fileName = strings[1];  // -> maven.pdf
            String extStorageDirectory = Environment.getExternalStorageDirectory().toString();
            File folder = new File(extStorageDirectory, "brandAmbassador");
            folder.mkdir();

            File pdfFile = new File(folder, fileName);

            try{
                pdfFile.createNewFile();
            }catch (IOException e){
                e.printStackTrace();
            }
            FileDownloader.downloadFile(fileUrl, pdfFile);
            Log.e("path", pdfFile.getAbsolutePath());
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            Log.e("end", "process");
            File pdfFile = new File(Environment.getExternalStorageDirectory() + "/brandAmbassador/" + "brand.pdf");
            Intent intent = new Intent(Intent.ACTION_VIEW);
            intent.setDataAndType(Uri.fromFile(pdfFile), "application/pdf");
            intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
            startActivity(intent);
        }
    }
}
