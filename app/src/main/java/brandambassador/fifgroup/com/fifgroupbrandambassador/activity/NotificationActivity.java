package brandambassador.fifgroup.com.fifgroupbrandambassador.activity;

import android.content.BroadcastReceiver;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bilinedev.coreframework.connection.ServiceResponse;
import com.bilinedev.coreframework.util.DialogUtil;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import brandambassador.fifgroup.com.fifgroupbrandambassador.BaseActivity;
import brandambassador.fifgroup.com.fifgroupbrandambassador.R;
import brandambassador.fifgroup.com.fifgroupbrandambassador.adapter.HistoryAdapter;
import brandambassador.fifgroup.com.fifgroupbrandambassador.adapter.NotificationAdapter;
import brandambassador.fifgroup.com.fifgroupbrandambassador.connection.Filter;
import brandambassador.fifgroup.com.fifgroupbrandambassador.model.HistoryModel;
import brandambassador.fifgroup.com.fifgroupbrandambassador.model.NotificationModel;
import brandambassador.fifgroup.com.fifgroupbrandambassador.util.UserPreference;
import butterknife.Bind;

/**
 * Created by air-water on 2/5/17.
 */

public class NotificationActivity extends BaseActivity {

    NotificationAdapter adapter;
    @Bind(R.id.rvListNotification) RecyclerView rvListNotification;
    @Bind(R.id.pbNotification) ProgressBar pbNotification;
    @Bind(R.id.view_emtpy_data) View viewEmptyData;
    @Bind(R.id.message) TextView txtMessage;

    UserPreference userPreference;
    BroadcastReceiver receiver = getReceiver();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setNavigationBackText(this, getString(R.string.t_notification));

        userPreference = UserPreference.getInstance(this);

        adapter = new NotificationAdapter(this);
        rvListNotification.setLayoutManager(new LinearLayoutManager(this));
        rvListNotification.setHasFixedSize(true);
        rvListNotification.setAdapter(adapter);

        postReadNotif(getParam());
    }

    private HashMap<String, String> getParam(){
        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put("id", userPreference.getUserId());
        return hashMap;
    }

    @Override
    protected int getResourceLayout() {
        return R.layout.activity_notification;
    }

    @Override
    protected void onResume() {
        super.onResume();
        registerReceiver(receiver, new IntentFilter(Filter.GET_NOTIFICATION));
        registerReceiver(receiver, new IntentFilter(Filter.POST_NOTIFICATION));
    }

    @Override
    protected void onStop() {
        super.onStop();
        unregisterReceiver(receiver);
    }

    @Override
    public void successResponse(ServiceResponse response, String filter) {
        super.successResponse(response, filter);
        switch (filter){
            case Filter.POST_NOTIFICATION:
                getNotification(getParam());
                break;
            case Filter.GET_NOTIFICATION:
                rvListNotification.setVisibility(View.VISIBLE);
                try {
                    JSONObject object = new JSONObject(response.getContent());
                    JSONObject jsonObjectData = object.getJSONObject("data");
                    JSONArray jsonArrayData = jsonObjectData.getJSONArray("data");
                    ArrayList<NotificationModel> notificationModels = new ArrayList<>();
                    if (jsonArrayData.length() > 0){
                        for (int i = 0; i < jsonArrayData.length(); i++) {
                            JSONObject objectData = jsonArrayData.getJSONObject(i);
                            NotificationModel notificationModel = new NotificationModel();
                            notificationModel.setTitle(objectData.getString("description"));
                            notificationModel.setDate(objectData.getString("created"));
                            notificationModels.add(notificationModel);
                        }
                        adapter.update(notificationModels);
                        adapter.setNotificationListener(listener);
                    } else {
                        viewEmptyData.setVisibility(View.VISIBLE);
                        txtMessage.setText(getString(R.string.msg_no_notification));
                    }

                    pbNotification.setVisibility(View.GONE);
                } catch (JSONException e){
                    e.printStackTrace();
                }
                break;
        }

    }

    @Override
    public void badResponse(ServiceResponse response, String filter) {
        super.badResponse(response, filter);
        pbNotification.setVisibility(View.GONE);
        DialogUtil.messageDialog(getSupportFragmentManager(), response.getMessage());
    }

    @Override
    public void onTimeout(String filter) {
        super.onTimeout(filter);
        pbNotification.setVisibility(View.GONE);
        DialogUtil.messageDialog(getSupportFragmentManager(), getString(R.string.msg_rto));
    }

    NotificationAdapter.NotificationListener listener = new NotificationAdapter.NotificationListener() {
        @Override
        public void onClick(NotificationModel notificationModel) {
            Intent intent = new Intent(NotificationActivity.this, HistoryActivity.class);
            intent.putExtra("reward_points", getIntent().getStringExtra("reward_points"));
            intent.putExtra("hebat_points", getIntent().getStringExtra("hebat_points"));
            startActivity(intent);
        }
    };

    public void dummy(){
        ArrayList<NotificationModel> notificationModels = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            NotificationModel notificationModel = new NotificationModel();
            notificationModel.setDate("19 Jan");
            notificationModel.setTitle("Lorem Ipsum Dolor Lorem Ipsum");
            notificationModels.add(notificationModel);
        }
        adapter.update(notificationModels);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (getIntent().getStringExtra("type") != null){
            startActivity(new Intent(NotificationActivity.this, DashboardActivity.class));
        }
    }

}
