package brandambassador.fifgroup.com.fifgroupbrandambassador.connection;

/**
 * Created by telolet on 10/18/16.
 */

public class Filter {

    /**LOGIN*/
    public static final String GET_REGISTER = "getRegister";
    public static final String GET_LOGIN = "getLogin";
    public static final String GET_FORGOT_PASSWORD = "getForgotPassword";
    public static final String GET_PROFILE = "getProfile";

    public static final String GET_PDF = "getPDF";
    public static final String GET_NOTIFICATION = "getNotification";
    public static final String POST_NOTIFICATION = "postNotification";
    public static final String GET_HISTORY = "getHistory";
    public static final String GET_REDEEM = "getRedeem";
    public static final String GET_LIST_REDEEM = "getListRedeem";

    public static final String POST_BRAND_DETECTIVE = "brandDetective";
    public static final String GET_BRAND_CHALLENGE = "brandChallenge";
    public static final String POST_BRAND_CHALLENGE = "postBrandChallenge";
    public static final String POST_BRAND_POLICE = "brandPolice";


}
