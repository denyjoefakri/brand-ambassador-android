package brandambassador.fifgroup.com.fifgroupbrandambassador;

import android.app.Activity;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import brandambassador.fifgroup.com.fifgroupbrandambassador.connection.ServiceActivity;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by telolet on 21/11/16.
 */

public abstract class BaseActivity extends ServiceActivity {

    public Typeface fontRegular;
    public Typeface fontBold;
    public Typeface fontBoldItalic;
    public Typeface fontItalic;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getResourceLayout());
        ButterKnife.bind(this);
        fontRegular = Typeface.createFromAsset(getAssets(), getString(R.string.font_regular));
        fontBold = Typeface.createFromAsset(getAssets(), getString(R.string.font_bold));
        fontBoldItalic = Typeface.createFromAsset(getAssets(), getString(R.string.font_bolditalic));
        fontItalic = Typeface.createFromAsset(getAssets(), getString(R.string.font_italic));
    }

    protected abstract int getResourceLayout();

    /**
     * Navigation
     */
    public ImageView toolbarIcon;
    public ImageView toolbarIconRight;
    public TextView toolbarMenu;
    public TextView toolbarTitleText;


    public void initToolbarText() {
        toolbarIcon = (ImageView) findViewById(R.id.toolbar_icon);
        toolbarTitleText = (TextView) findViewById(R.id.toolbar_title_text);
        toolbarMenu = (TextView) findViewById(R.id.toolbar_menu);
        toolbarTitleText.setTypeface(fontBold);
        toolbarMenu.setTypeface(fontRegular);
    }

    public void initToolbarProfile() {
        toolbarIcon = (ImageView) findViewById(R.id.toolbar_icon);
        toolbarIconRight = (ImageView) findViewById(R.id.toolbar_icon_right);
        toolbarTitleText = (TextView) findViewById(R.id.toolbar_title_text);
        toolbarTitleText.setTypeface(fontBold);
    }

    public void setNavigationBackText(final Activity activity, String title) {
        initToolbarText();
        toolbarTitleText.setText(title);
        toolbarIcon.setImageResource(R.drawable.ic_keyboard_arrow_left);
        toolbarIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                activity.finish();
            }
        });
    }

    public void setNavigationProfile(final Activity activity, String title, View.OnClickListener listener) {
        initToolbarProfile();
        toolbarTitleText.setText(title);
        toolbarIcon.setImageResource(R.drawable.ic_keyboard_arrow_left);
        toolbarIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                activity.finish();
            }
        });
        toolbarIconRight.setImageResource(R.drawable.ic_exit_to_app_white_24dp);
        toolbarIconRight.setOnClickListener(listener);
    }

    public void setNavigationBackText(String title, View.OnClickListener onClickListener) {
        initToolbarText();
        toolbarTitleText.setText(title);
        toolbarIcon.setImageResource(R.drawable.ic_keyboard_arrow_left);
        toolbarIcon.setOnClickListener(onClickListener);
    }


}
