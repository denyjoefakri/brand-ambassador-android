package brandambassador.fifgroup.com.fifgroupbrandambassador.activity;

import android.content.BroadcastReceiver;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bilinedev.coreframework.connection.ServiceResponse;
import com.bilinedev.coreframework.util.DialogUtil;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import brandambassador.fifgroup.com.fifgroupbrandambassador.BaseActivity;
import brandambassador.fifgroup.com.fifgroupbrandambassador.R;
import brandambassador.fifgroup.com.fifgroupbrandambassador.adapter.BrandChallengeAdapter;
import brandambassador.fifgroup.com.fifgroupbrandambassador.adapter.HistoryAdapter;
import brandambassador.fifgroup.com.fifgroupbrandambassador.connection.Filter;
import brandambassador.fifgroup.com.fifgroupbrandambassador.model.HistoryModel;
import brandambassador.fifgroup.com.fifgroupbrandambassador.util.UserPreference;
import butterknife.Bind;
import butterknife.OnClick;

/**
 * Created by air-water on 2/5/17.
 */

public class HistoryActivity extends BaseActivity {

    HistoryAdapter adapter;
    @Bind(R.id.rvListHistory) RecyclerView rvListHistory;
    @Bind(R.id.pbHistory) ProgressBar pbHistory;
    @Bind(R.id.txtRewardPoint) TextView txtRewardPoint;
    @Bind(R.id.txtHebatPoint) TextView txtHebatPoint;
    @Bind(R.id.view_emtpy_data) View viewEmptyData;
    @Bind(R.id.message) TextView txtMessage;

    BroadcastReceiver receiver = getReceiver();
    UserPreference userPreference;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setNavigationBackText(this, getString(R.string.t_brand_history));

        userPreference = UserPreference.getInstance(this);

        adapter = new HistoryAdapter(this);
        rvListHistory.setLayoutManager(new LinearLayoutManager(this));
        rvListHistory.setHasFixedSize(true);
        rvListHistory.setAdapter(adapter);

        if (getIntent().getStringExtra("reward_points") != null || getIntent().getStringExtra("hebat_points") != null){
            txtRewardPoint.setText(getIntent().getStringExtra("reward_points") + " " + getString(R.string.msg_points));
            txtHebatPoint.setText(getIntent().getStringExtra("hebat_points") + " " + getString(R.string.msg_points));
        }

        HashMap<String, String> map = new HashMap<>();
        map.put("id", userPreference.getUserId());
        getHistory(map);
    }

    @Override
    protected int getResourceLayout() {
        return R.layout.activity_history;
    }

    @Override
    protected void onResume() {
        super.onResume();
        registerReceiver(receiver, new IntentFilter(Filter.GET_HISTORY));
    }

    @Override
    protected void onStop() {
        super.onStop();
        unregisterReceiver(receiver);
    }

    @Override
    public void successResponse(ServiceResponse response, String filter) {
        super.successResponse(response, filter);
        pbHistory.setVisibility(View.GONE);
        try {
            JSONObject object = new JSONObject(response.getContent());
            JSONArray jsonArray = object.getJSONArray("data");
            ArrayList<HistoryModel> historyModels = new ArrayList<>();
            if (jsonArray.length() > 0 ){
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject objectData = jsonArray.getJSONObject(i);
                    HistoryModel historyModel = new HistoryModel();
                    historyModel.setTitle(objectData.getString("description"));
                    historyModel.setDate(objectData.getString("created"));
                    historyModel.setTypeBrands(objectData.getString("type"));
                    historyModels.add(historyModel);
                }
                adapter.update(historyModels);
            } else {
                viewEmptyData.setVisibility(View.VISIBLE);
                txtMessage.setText(getString(R.string.msg_no_history));
            }

        } catch (JSONException e){
            e.printStackTrace();
        }
    }

    @Override
    public void badResponse(ServiceResponse response, String filter) {
        super.badResponse(response, filter);
        pbHistory.setVisibility(View.GONE);
        DialogUtil.messageDialog(getSupportFragmentManager(), response.getMessage());
    }

    @Override
    public void onTimeout(String filter) {
        super.onTimeout(filter);
        pbHistory.setVisibility(View.GONE);
        DialogUtil.messageDialog(getSupportFragmentManager(), getString(R.string.msg_rto));
    }


}
