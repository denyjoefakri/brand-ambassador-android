package brandambassador.fifgroup.com.fifgroupbrandambassador.model;

/**
 * Created by air-water on 2/13/17.
 */

public class Dummy {

    public static String brandChallenge = "{\"status\":\"success\",\"msg\":\"\",\"data\":[{\"id\":\"1\",\"question\":\"Apa warna primer dari brand FIF Group?\",\"answer\":[{\"id\":\"1\",\"bc_id\":\"1\",\"bc_answer\":\"merah\",\"bc_answer_alphabet\":\"a\",\"created\":\"2017-02-12 19:59:51\",\"modified\":\"2017-02-12 20:40:03\",\"deleted\":\"0\"},{\"id\":\"2\",\"bc_id\":\"1\",\"bc_answer\":\"kuning\",\"bc_answer_alphabet\":\"b\",\"created\":\"2017-02-12 20:00:04\",\"modified\":\"2017-02-12 20:40:03\",\"deleted\":\"0\"},{\"id\":\"3\",\"bc_id\":\"1\",\"bc_answer\":\"biru\",\"bc_answer_alphabet\":\"c\",\"created\":\"2017-02-12 20:00:11\",\"modified\":\"2017-02-12 20:40:03\",\"deleted\":\"0\"},{\"id\":\"4\",\"bc_id\":\"1\",\"bc_answer\":\"hijau\",\"bc_answer_alphabet\":\"d\",\"created\":\"2017-02-12 20:00:22\",\"modified\":\"2017-02-12 20:40:03\",\"deleted\":\"0\"}],\"period_start\":\"2017-02-01 00:00:00\",\"period_end\":\"2017-02-28 00:00:00\",\"point\":\"20\",\"type\":\"text\",\"created_on\":\"2017-02-12 19:59:12\",\"modified_on\":null,\"deleted\":\"0\"}]}";
}
