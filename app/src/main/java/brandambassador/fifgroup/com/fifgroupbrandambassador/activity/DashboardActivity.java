package brandambassador.fifgroup.com.fifgroupbrandambassador.activity;

import android.content.BroadcastReceiver;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bilinedev.coreframework.connection.ServiceResponse;
import com.bilinedev.coreframework.util.DialogUtil;
import com.bumptech.glide.Glide;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

import brandambassador.fifgroup.com.fifgroupbrandambassador.BaseActivity;
import brandambassador.fifgroup.com.fifgroupbrandambassador.R;
import brandambassador.fifgroup.com.fifgroupbrandambassador.connection.Filter;
import brandambassador.fifgroup.com.fifgroupbrandambassador.model.ProfileModel;
import brandambassador.fifgroup.com.fifgroupbrandambassador.util.Konstan;
import brandambassador.fifgroup.com.fifgroupbrandambassador.util.UserPreference;
import brandambassador.fifgroup.com.fifgroupbrandambassador.util.Util;
import butterknife.Bind;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by air-water on 2/5/17.
 */

public class DashboardActivity extends BaseActivity {

    @Bind(R.id.mainView) LinearLayout linearLayout;
    @Bind(R.id.txtCountOne) TextView txtCountOne;
    @Bind(R.id.txtCountTwo) TextView txtCountTwo;
    @Bind(R.id.pbNotification) ProgressBar pbNotification;
    @Bind(R.id.pbProfile) ProgressBar pbProfile;

    @Bind(R.id.view_profile) LinearLayout viewProfile;
    @Bind(R.id.imgProfile) CircleImageView imgProfile;
    @Bind(R.id.txtName) TextView txtName;
    @Bind(R.id.txtRewardPoint) TextView txtRewardPoint;
    @Bind(R.id.txtHebatPoint) TextView txtHebatPoint;

    @Bind(R.id.btnBrandDetective) ImageView btnBrandDetective;
    @Bind(R.id.btnBrandPolice) ImageView btnBrandPolice;
    @Bind(R.id.btnBrandChallenge) ImageView btnBrandChallenge;
    @Bind(R.id.btnDesignCenter) ImageView btnDesignCenter;

    static DashboardActivity dashboardActivity;
    UserPreference userPreference;
    BroadcastReceiver receiver = getReceiver();

    ProfileModel profileModel;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        dashboardActivity = this;
        userPreference = UserPreference.getInstance(this);
        initComponents();
        initButton();
    }

    @Override
    protected int getResourceLayout() {
        return R.layout.activity_dashboard;
    }

    @Override
    public void initComponents() {
        super.initComponents();
        if (getIntent().getStringExtra(Konstan.MESSAGE) != null)
            Util.initSnackbar(this, linearLayout, R.string.msg_success_login, listener, "OK");
    }

    private HashMap<String,String> getParam(){
        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put("id", userPreference.getUserId());
        return hashMap;
    }

    private void initButton(){
        Util.setImage(this, R.drawable.brand_police_dashboard, btnBrandPolice);
        Util.setImage(this, R.drawable.brand_challenge_dashboard, btnBrandChallenge);
        Util.setImage(this, R.drawable.brand_detective_dashboard, btnBrandDetective);
        Util.setImage(this, R.drawable.design_center_dashboard, btnDesignCenter);
    }

    @Override
    protected void onResume() {
        super.onResume();
        getNotification(getParam());
        getProfile(getParam());
        registerReceiver(receiver, new IntentFilter(Filter.GET_NOTIFICATION));
        registerReceiver(receiver, new IntentFilter(Filter.GET_PROFILE));
    }

    @Override
    protected void onStop() {
        super.onStop();
        unregisterReceiver(receiver);
    }

    @Override
    public void successResponse(ServiceResponse response, String filter) {
        super.successResponse(response, filter);
        try {
            JSONObject object = new JSONObject(response.getContent());
            JSONObject dataObject = object.getJSONObject("data");
            switch (filter){
                case Filter.GET_NOTIFICATION:
                    pbNotification.setVisibility(View.GONE);
                    if (dataObject.getString("unread").length() >= 10) {
                        txtCountTwo.setVisibility(View.VISIBLE);
                        txtCountTwo.setText(dataObject.getString("unread"));
                    } else{
                        txtCountOne.setVisibility(View.VISIBLE);
                        txtCountOne.setText(dataObject.getString("unread"));
                    }
                    break;
                case Filter.GET_PROFILE:
                    pbProfile.setVisibility(View.GONE);
                    viewProfile.setVisibility(View.VISIBLE);

                    profileModel = new ProfileModel();
                    profileModel.setId(dataObject.getString("id"));
                    profileModel.setAvatar(dataObject.getString("avatar"));
                    profileModel.setEmail(dataObject.getString("email"));
                    String name = Character.toUpperCase(dataObject.getString("fullname").charAt(0)) + dataObject.getString("fullname").substring(1);
                    profileModel.setFullname(name);
                    profileModel.setNpk(dataObject.getString("npk"));
                    profileModel.setCabang(dataObject.getString("cabang"));
                    profileModel.setPhone(dataObject.getString("phone"));
                    profileModel.setPoint(dataObject.getString("point"));
                    profileModel.setPoint_total(dataObject.getString("point_total"));
                    profileModel.setActive(dataObject.getString("active"));

                    Glide.with(DashboardActivity.this).load(profileModel.getAvatar()).error(R.drawable.ic_profile).into(imgProfile);
                    txtName.setText(profileModel.getFullname());
                    txtRewardPoint.setText(profileModel.getPoint()+ " " + getString(R.string.msg_points));
                    txtHebatPoint.setText(profileModel.getPoint_total() + " " + getString(R.string.msg_points));
                    break;
            }
        } catch (JSONException e){

        }


    }

    @Override
    public void badResponse(ServiceResponse response, String filter) {
        super.badResponse(response, filter);
        pbNotification.setVisibility(View.GONE);
        pbProfile.setVisibility(View.GONE);
        DialogUtil.messageDialog(getSupportFragmentManager(), response.getMessage());
    }

    @Override
    public void onTimeout(String filter) {
        super.onTimeout(filter);
        pbNotification.setVisibility(View.GONE);
        pbProfile.setVisibility(View.GONE);
        DialogUtil.messageDialog(getSupportFragmentManager(), getString(R.string.msg_rto));
    }

    @OnClick(R.id.btnNotification)
    public void notification(){
        Intent intent = new Intent(DashboardActivity.this, NotificationActivity.class);
        intent.putExtra("reward_points", profileModel.getPoint());
        intent.putExtra("hebat_points", profileModel.getPoint_total());
        startActivity(intent);
    }

    @OnClick (R.id.btnBrandChallenge)
    public void challenge(){
        startActivity(new Intent(DashboardActivity.this, BrandChallengeActivity.class));
    }

    @OnClick (R.id.btnBrandDetective)
    public void detective(){
        startActivity(new Intent(DashboardActivity.this, BrandDetectiveActivity.class));
    }

    @OnClick (R.id.btnBrandPolice)
    public void police(){
        startActivity(new Intent(DashboardActivity.this, BrandPoliceActivity.class));
    }

    @OnClick (R.id.btnDesignCenter)
    public void designCenter(){
        startActivity(new Intent(DashboardActivity.this, DesignCenterActivity.class));
    }

    @OnClick (R.id.btnProfile)
    public void profile(){
        Intent intent = new Intent(DashboardActivity.this, ProfileActivity.class);
        intent.putExtra("fullname", profileModel.getFullname());
        intent.putExtra("email", profileModel.getEmail());
        intent.putExtra("npk", profileModel.getNpk());
        intent.putExtra("cabang", profileModel.getCabang());
        intent.putExtra("phone", profileModel.getPhone());
        intent.putExtra("avatar", profileModel.getAvatar());
        startActivity(intent);
    }

    @OnClick (R.id.btnHistory)
    public void history(){
        Intent intent = new Intent(DashboardActivity.this, HistoryActivity.class);
        intent.putExtra("reward_points", profileModel.getPoint());
        intent.putExtra("hebat_points", profileModel.getPoint_total());
        startActivity(intent);
    }

    @OnClick (R.id.btnRedeem)
    public void redeem(){
        Intent intent = new Intent(DashboardActivity.this, RedeemActivity.class);
        intent.putExtra("reward_points", profileModel.getPoint());
        startActivity(intent);
    }

    View.OnClickListener listener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

        }
    };

    public static DashboardActivity getIntance(){
        return dashboardActivity;
    }


}
