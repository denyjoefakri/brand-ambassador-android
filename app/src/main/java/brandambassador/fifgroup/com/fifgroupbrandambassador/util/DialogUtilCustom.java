package brandambassador.fifgroup.com.fifgroupbrandambassador.util;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Build;
import android.support.v4.app.FragmentManager;
import android.view.Window;

import com.bilinedev.coreframework.ui.dialog.ConfirmationDialog;
import com.bilinedev.coreframework.ui.dialog.MessageDialog;
import com.bilinedev.coreframework.ui.dialog.MessageWithTitleDialog;

import brandambassador.fifgroup.com.fifgroupbrandambassador.dialog.DialogForgotPassword;
import brandambassador.fifgroup.com.fifgroupbrandambassador.dialog.DialogRedeem;
import brandambassador.fifgroup.com.fifgroupbrandambassador.dialog.MessageDialogAnswer;

/**
 * Created by pristyanchandra on 10/14/16.
 */

public class DialogUtilCustom {
    public static ProgressDialog createProgressDialog(Context context, String message, boolean cancelable) {
        ProgressDialog dialog = new ProgressDialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        }
        dialog.setMessage(message);
        dialog.setCancelable(cancelable);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
        return dialog;
    }

    public static MessageDialogAnswer messageDialog(FragmentManager fragmentManager, String message) {
        MessageDialogAnswer dialog = MessageDialogAnswer.newIntance(message);
        dialog.show(fragmentManager, "dialogMessage");
        return dialog;
    }

    public static DialogForgotPassword forgotPasswordDialog (FragmentManager fragmentManager, String message) {
        DialogForgotPassword dialog = DialogForgotPassword.newIntance(message);
        dialog.show(fragmentManager, "dialogMessage");
        return dialog;
    }

    public static DialogRedeem dialogRedeem (FragmentManager fragmentManager, String message) {
        DialogRedeem dialog = DialogRedeem.newIntance(message);
        dialog.show(fragmentManager, "dialogMessage");
        return dialog;
    }
}
