package brandambassador.fifgroup.com.fifgroupbrandambassador.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import brandambassador.fifgroup.com.fifgroupbrandambassador.R;
import brandambassador.fifgroup.com.fifgroupbrandambassador.activity.HistoryActivity;
import brandambassador.fifgroup.com.fifgroupbrandambassador.model.HistoryModel;
import brandambassador.fifgroup.com.fifgroupbrandambassador.model.NotificationModel;
import brandambassador.fifgroup.com.fifgroupbrandambassador.util.Util;
import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by pristyanchandra on 1/25/17.
 */

public class NotificationAdapter extends RecyclerView.Adapter {

    private ArrayList<NotificationModel> notificationModels;
    NotificationListener notificationListener;
    Context context;

    public NotificationAdapter(Context context) {
        this.context = context;
        notificationModels = new ArrayList<>();
    }

    public void update(ArrayList<NotificationModel> notificationModels) {
        this.notificationModels = notificationModels;
        notifyDataSetChanged();
    }

    public void setNotificationListener(NotificationListener notificationListener){
        this.notificationListener = notificationListener;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_notification, parent, false);
        return new Holder(v);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ((Holder) holder).setValue(notificationModels.get(position));
    }

    @Override
    public int getItemCount() {
        return notificationModels.size();
    }

    public class Holder extends RecyclerView.ViewHolder {
        @Bind(R.id.txtDate) TextView txtDate;
        @Bind(R.id.txtTitle) TextView txtTitle;

        Holder(View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
        }

        private void setValue(final NotificationModel notificationModel) {
            txtTitle.setText(notificationModel.getTitle());
            String[] getDate = notificationModel.getDate().split(" ");
            String[] date = getDate[0].split("-");

            txtDate.setText(date[2] + " " + Util.getMonth(Integer.parseInt(date[1])) + " " + date[0]);
            //txtDate.setText(notificationModel.getDate());

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    notificationListener.onClick(notificationModel);
                }
            });
        }
    }

    public interface NotificationListener{
        void onClick(NotificationModel notificationModel);
    }

}
