package brandambassador.fifgroup.com.fifgroupbrandambassador.util;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by pristyanchandra on 12/30/16.
 */

public class UserPreference {
    private static final String USER_ID = "user_id";
    private static final String USER_EMAIL = "user_email";
    private static final String USER_FULL_NAME = "user_fullname";
    private static final String USER_NPK = "user_npk";
    private static final String DOWNLOADING = "downloading";


    private SharedPreferences prefs;
    Context context;
    private static UserPreference helper;
    private SharedPreferences.Editor editor;

    public static UserPreference getInstance(Context context) {
        helper = new UserPreference(context);
        return helper;
    }

    private UserPreference(Context context) {
        this.context = context;
        prefs = context.getSharedPreferences("Quanto", Context.MODE_PRIVATE);
        editor = prefs.edit();
    }

    public void setUserId(String data) {
        editor.putString(USER_ID, data);
        editor.commit();
    }

    public String getUserId() {
        return prefs.getString(USER_ID, null);
    }

    public void setUserEmail(String data) {
        editor.putString(USER_EMAIL, data);
        editor.commit();
    }

    public String getUserEmail() {
        return prefs.getString(USER_EMAIL, null);
    }

    public void setUserFullName(String data) {
        editor.putString(USER_FULL_NAME, data);
        editor.commit();
    }

    public String getUserFullName() {
        return prefs.getString(USER_FULL_NAME, null);
    }

    public void setUserNPK(String data) {
        editor.putString(USER_NPK, data);
        editor.commit();
    }

    public String getUserNpk() {
        return prefs.getString(USER_NPK, null);
    }

    public void setDownloading(boolean data) {
        editor.putBoolean(DOWNLOADING, data);
        editor.commit();
    }

    public boolean getDownloading() {
        return prefs.getBoolean(DOWNLOADING, false);
    }
}
