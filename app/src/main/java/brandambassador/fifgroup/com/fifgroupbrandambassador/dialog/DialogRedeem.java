package brandambassador.fifgroup.com.fifgroupbrandambassador.dialog;

import android.content.BroadcastReceiver;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;

import com.bilinedev.coreframework.connection.ServiceResponse;
import com.bilinedev.coreframework.ui.widget.spinner.SingleSpinnerAdapter;
import com.bilinedev.coreframework.util.DialogUtil;
import com.bumptech.glide.Glide;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import brandambassador.fifgroup.com.fifgroupbrandambassador.R;
import brandambassador.fifgroup.com.fifgroupbrandambassador.connection.Filter;
import brandambassador.fifgroup.com.fifgroupbrandambassador.connection.Path;
import brandambassador.fifgroup.com.fifgroupbrandambassador.connection.ServiceDialog;


/**
 * Created by pristyanchandra on 10/14/16.
 */
public class DialogRedeem extends ServiceDialog {

    private OnRedeem redeem;
    BroadcastReceiver receiver = getReceiver();
    LayoutInflater layoutInflater;

    ProgressBar pbList;
    Spinner listRedeem;

    ArrayList<String> point;

    public static DialogRedeem newIntance(String message) {
        DialogRedeem messageDialog = new DialogRedeem();
        Bundle bundle = new Bundle();
        bundle.putCharSequence("message", message);
        messageDialog.setArguments(bundle);
        return messageDialog;
    }

    public void setOnRedeem(OnRedeem redeem) {
        this.redeem = redeem;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        this.layoutInflater = inflater;
        return inflater.inflate(R.layout.dialog_redeem, container, false);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NO_TITLE, R.style.CustomDialog);
    }

    @Override
    public void onViewCreated(View v, @Nullable Bundle savedInstanceState) {
        getDialog().requestWindowFeature(Window.FEATURE_NO_TITLE);
        setStyle(STYLE_NO_FRAME, 0);

        point = new ArrayList<>();
        final EditText edPoint = (EditText) v.findViewById(R.id.edPoint);
        pbList = (ProgressBar) v.findViewById(R.id.pbListRedeem);
        listRedeem = (Spinner) v.findViewById(R.id.spnListRedeem);

        ImageView img = (ImageView) v.findViewById(R.id.imgPrice);
        Glide.with(this).load(Path.IMAGE_REDEEM).error(R.drawable.image_landscape).into(img);

        //edPoint.setText(point.get(listRedeem.getSelectedItemPosition()));

        Button btnPoint = (Button) v.findViewById(R.id.btnRedeem);
        btnPoint.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (redeem != null) {
                    redeem.onRedeem(edPoint.getText().toString());
                }
                dismiss();
            }
        });

        getListRedeem();

    }

    public interface OnRedeem {
        public void onRedeem(String point);
    }

    @Override
    public void onResume() {
        super.onResume();
        getActivity().registerReceiver(receiver, new IntentFilter(Filter.GET_LIST_REDEEM));
    }

    @Override
    public void onPause() {
        super.onPause();
        getActivity().unregisterReceiver(receiver);
    }

    @Override
    public void successResponse(ServiceResponse response, String filter) {
        super.successResponse(response, filter);
        pbList.setVisibility(View.GONE);
        try {
            JSONObject jsonObject = new JSONObject(response.getContent());
            JSONArray jsonArray = jsonObject.getJSONArray("data");
            ArrayList<String> spin = new ArrayList<>();
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject object = jsonArray.getJSONObject(i);
                spin.add(object.getString("name"));
                point.add(object.getString("point"));
            }
            listRedeem.setAdapter(new SingleSpinnerAdapter(getActivity(), layoutInflater, spin));

        }catch (JSONException e){

        }


    }

    @Override
    public void badResponse(ServiceResponse response, String filter) {
        super.badResponse(response, filter);
        pbList.setVisibility(View.GONE);
        DialogUtil.messageDialog(getFragmentManager(), response.getMessage());
    }

    @Override
    public void onTimeout(String filter) {
        super.onTimeout(filter);
        pbList.setVisibility(View.GONE);
        DialogUtil.messageDialog(getFragmentManager(), getString(R.string.msg_rto));
    }
}